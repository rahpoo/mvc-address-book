# MVC Address Book

This is a simple address book that coding in a simple MVC method and has the following features.

I tried build this app base on the SOLID and KISS principles also using some design patterns such as Singleton.
Also used PSRs recommendation to has a clean code and dynamic project.

- Dynamic routing with method control
- Template handling
- Ajax requests
- Config manager
- Component base
- IoC Container
- Model driver
- Html Helpers
