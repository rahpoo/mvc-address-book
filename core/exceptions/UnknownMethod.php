<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 12:49 AM
 */

namespace core\exceptions;


class UnknownMethod extends \Exception
{
    public function getName()
    {
        return 'Unknown Property';
    }
}