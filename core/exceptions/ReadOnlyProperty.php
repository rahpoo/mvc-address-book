<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/29/2018
 * Time: 8:00 PM
 */

namespace core\exceptions;


use Throwable;

class ReadOnlyProperty extends \Exception
{
    public function __construct(string $class = "", string $name = "", int $code = 0, Throwable $previous = null)
    {
        $message = "The Property `$name` of `$class` is Read-Only property!";
        parent::__construct($message, $code, $previous);
    }
}