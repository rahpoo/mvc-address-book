<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/2/2018
 * Time: 2:31 AM
 */

namespace core\exceptions;


use Throwable;

class DatabaseException extends \Exception
{
    public function __construct(\mysqli_stmt $stmt, int $code = 0, Throwable $previous = null)
    {
        $message = "Database Syntax Error! {#$stmt->errno}: {$stmt->error}";
        parent::__construct($message, $code, $previous);
    }
}