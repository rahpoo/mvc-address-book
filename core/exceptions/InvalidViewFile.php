<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 12:54 AM
 */

namespace core\exceptions;


class InvalidViewFile extends \Exception
{
    public function getName()
    {
        return 'Invalid Template View File';
    }
}