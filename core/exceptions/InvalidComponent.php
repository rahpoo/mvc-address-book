<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 2:07 AM
 */

namespace core\exceptions;


class InvalidComponent extends \Exception
{
    public function getName()
    {
        return 'Invalid Component/Definition';
    }
}