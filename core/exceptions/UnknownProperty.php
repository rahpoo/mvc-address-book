<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 12:46 AM
 */

namespace core\exceptions;

class UnknownProperty extends \Exception
{
    public function __construct(string $class = "", string $name = "", int $code = 0, Throwable $previous = null)
    {
        $message = "Called Unknown Property `$name` in the class `$class`!";
        parent::__construct($message, $code, $previous);
    }
}