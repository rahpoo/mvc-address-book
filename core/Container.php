<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 1:10 AM
 */

namespace core;


use core\base\Component;
use core\exceptions\InvalidComponent;

/**
 * Class Container.
 * This is an Dependency Injection Container to manage injecting and reading objects and third party libraries.
 * @package core
 */
class Container extends Singleton
{
    private $instances = [];

    public function has($name)
    {

    }

    /**
     * @param string $name
     * @param string|array $definition
     * @param array $params
     * @param bool $is_singleton
     * @return mixed|object
     * @throws \Exception
     */
    public function assign(string $name, $definition, array $params = [], bool $is_singleton = true)
    {
        if (isset($this->instances[$name]))
            return $this->get($name);

        if (is_array($definition)) {
            if (!isset($definition['class']))
                throw new \Exception('the definition should has a`class` index!');

            $class = $definition['class'];
            unset($definition['class']);
            $params = $definition;
            $definition = $class;
        }

        $this->instances[$name] = [
            'class' => $definition,
            'singleton' => $is_singleton,
            'params' => $params
        ];

        return $this->get($name);
    }

    /**
     * @param string $name
     * @param string|array $definition
     * @param array $params
     * @return mixed|object
     * @throws \Exception
     */
    public function set(string $name, $definition, array $params = [])
    {
        if (is_array($definition)) {
            if (!isset($definition['class']))
                throw new \Exception('the definition should has a`class` index!');

            $class = $definition['class'];
            unset($definition['class']);
            $params = $definition;
            $definition = $class;
        }

        $this->instances[$name] = [
            'class' => $definition,
            'singleton' => false,
            'params' => $params
        ];

        return $this->get($name);
    }

    /**
     * @param $definition
     * @param array $params
     * @return mixed|object
     * @throws \Exception
     */
    public function get($definition, $params = [])
    {
        if (!isset($this->instances[$definition])) {
            if (!strstr($definition, '\\'))
                throw new \Exception('Concrete class namespace is invalid!');

            $this->set($definition);
        }

        if ($concrete = array_get($this->instances[$definition], 'class'))
            return $this->resolve($concrete, array_get($this->instances[$definition], 'params', $params));
    }

    /**
     * @param $concrete
     * @param array $params
     * @return mixed|object
     * @throws \Exception
     */
    public function resolve($concrete, $params = [])
    {
        if ($concrete instanceof \Closure)
            return $concrete($this, $params);

        if (!class_exists($concrete))
            throw new \ReflectionException("Class `$concrete` is not exists!");

        $reflector = new \ReflectionClass($concrete);
        if (!$reflector->isInstantiable())
            throw new \RuntimeException("Class `$concrete` is not instantiable!");

        $constructor = $reflector->getConstructor();
        if (is_null($constructor))
            return $reflector->newInstance();

        $implements = class_implements($concrete);

        if (!isset($implements['core\contracts\iComponent'])) {
            // Get parameters of concrete constructor
            $constructor_params = $constructor->getParameters();

            if (!empty($params)) {
                foreach ($constructor_params as $k => $cparam) {
                    if (isset($params[$cparam->name]))
                        $constructor_params[$k][$cparam->name] = $params[$cparam->name];
                }
            }

            $parameters = $this->getDependencies($constructor_params);

            return $reflector->newInstanceArgs($parameters);
        }

        return $reflector->newInstanceArgs([$params]);
    }

    /**
     * @param $parameters
     * @return array
     * @throws \Exception
     */
    public function getDependencies($parameters) : array
    {
        $dependencies = [];

        foreach ($parameters as $key => $parameter) {
            if (is_object($parameter) && $dependency = $parameter->getClass())
                $dependencies[$key] = $this->get($dependency->name);
            else
                $dependencies[$key] = $parameter;
        }

        return $dependencies;
    }



    /**
     * @param $name
     * @return mixed|object
     * @throws \Exception
     */
    public function __get($name)
    {
        if (isset($this->instances[$name]))
            return $this->get($name);

        throw new \Exception("Undefined component or concrete class `$name`");
    }
}