<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/8/2018
 * Time: 2:29 PM
 */

namespace core\widgets;


use core\base\Widget;
use core\contracts\iModel;
use core\helpers\html\BaseElement;
use core\helpers\html\form\Form;
use core\helpers\html\form\Label;

class FormWidget extends Widget
{
    public $autoLabel = true;

    public $fieldClass = 'form-control';

    public $errorPanelTemplate = '
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <h4>Please ensure to fix the following errors</h4>
        {errors}
    </div>
    ';

    public $fieldTemplate = '
    <div class="row">
        <div class="col-sm-12">{label}</div>
        <div class="col-sm-12">{field}</div>
    </div>
    ';

    public $buttonTemplate = '
    <div class="pull-left">
        {button}
    </div>
    ';

    /**
     * @var Form
     */
    protected $form;
    /**
     * @var iModel
     */
    protected $model;
    protected $fields = [];
    protected $fieldNamespace = 'core\\helpers\\html\\form\\';
    protected $buttons = [];

    /**
     * @param iModel $model
     * @param string $action
     * @param string $method
     * @param string $id
     * @param string $enc_type
     * @param array $options
     * @return FormWidget
     */
    public static function begin(iModel $model, string $action = '', string $method = 'POST', string $id = '', string $enc_type = '', array $options = [])
    {
        $self = static::getInstance();

        $self->model = $model;

        $options = array_merge([
            'action' => $action,
            'method' => $method,
            'id' => $id,
            'enctype' => $enc_type], $options);

        $self->form = Form::begin($options);

        if ($model->isNewRecord() !== true) {
            $primaryKey = $model::primaryKey();
            $self->field('hidden', $primaryKey, $model->{$primaryKey});
        }

        return $self;
    }

    public function field($type = 'text', $name = '', $value = null, $options = [])
    {
        $model = array_get($options, 'model', false);

        if ($model) {
            unset($options['model']);
        } else {
            $model = $this->model;
        }

        $field_id = 'field_' . $name;

        $options['class'] = $this->fieldClass . ' ' . array_get($options, 'class' , '');
        $options['id'] = $field_id;
        $options['name'] = $name;
        $options['value'] = isset($value) ? $value : $model->{$name};

        /**
         * @var BaseElement $fieldClass
         */
        $fieldClass = $this->fieldNamespace . ucfirst($type);
        if (class_exists($fieldClass)) {
            $field = $fieldClass::begin($options);
            $label = '';

            if ($field->hasLabel != false) {
                $label = Label::begin([
                    'for' => $field_id,
                    'label' => is_string($field->hasLabel) ? $field->hasLabel : $model->getFieldLabel($name)
                ]);
            }

            $this->fields[$name] = [
                'attribute' => $name,
                'id' => $field_id,
                'field' => $field,
                'label' => $label
            ];
            return $this->fields[$name]['field'];
        }

        return false;
    }

    /**
     * Add button to current form.
     * @param string $type
     * @param string $caption
     * @param array $options
     */
    public function button(string $type = 'submit', string $caption = 'Submit', array $options = array())
    {
        $fieldClass = 'btn';

        if ($customFieldClass = array_get($options, 'class')) {
            $fieldClass .= ' ' . $customFieldClass;
            unset($options['class']);
        } else {
            $fieldClass .= ' btn-success';
        }

        $options = array_merge(
            [
                'type' => $type,
                'caption' => $caption,
                'class' => $fieldClass
            ]
            , $options
        );

        $fieldClass = $this->fieldNamespace . 'Button';

        if (class_exists($fieldClass)) {
            $field = (new $fieldClass())::begin($options);
            $this->fields['button:' . count($this->fields)] = [
                'field' => $field
            ];
        }
    }

    public function renderErrors()
    {
        $output = '';
        $errors = $this->model->getErrors();

        if (!empty($errors)) {
            $output = '<div class="alert alert-danger alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <h3>The following errors have occurred!</h3>';

            foreach ($errors as $field => $error) {
                $other_errors = '';

                if ($field_error = $this->model->getFieldLabel($field))
                    $output .= '<strong>' . $this->model->getFieldLabel($field) . '</strong> field ';
                else
                    $output .= '<strong>Syntax Error!</strong> ';

                foreach ($error as $index => $message) {
                    if ($index == 0)
                        $output .= $message . '<br />';
                    else
                        $other_errors .= '<li>And ' . $message . '</li>';
                }

                if (!empty($other_errors))
                    $output .= '<ul>' . $other_errors. '</ul>';

                $output .= '<br />';
            }

            $output .=  '</div>';
        }

        return $output;
    }

    public function render($return = false)
    {
        $errors = $this->renderErrors();

        foreach ($this->fields as $index => $attribute) {
            if (substr($index, 0, 4) == 'button:') {
                $button = array_get($attribute, 'field', '');

                if (is_object($button))
                    $button = $button->render(true);

                $result = str_replace('{button}', $button, $this->buttonTemplate);
            } else {
                $field = array_get($attribute, 'field');
                $label = array_get($attribute, 'label', '');

                if (is_object($field)) {
                    $field = $field->render(true);
                }

                if (is_object($label)) {
                    $label = $label->render(true);
                }

                $result = str_replace(['{label}', '{field}'], [$label, $field], $this->fieldTemplate);
            }

            $this->form->append($result);
        }

        if (!$return)
            echo $errors;

        $result = $this->form->render($return);

        if ($return) {
            return $errors . $result;
        }

        return $this;
    }
}