<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/23/2018
 * Time: 10:55 PM
 */

namespace core\widgets;


use core\base\Widget;
use core\contracts\iModel;
use core\helpers\html\Link;
use core\helpers\html\table\Table;

class GridViewWidget extends Widget
{
    /**
     * @var Table
     */
    protected $table;
    /**
     * @var iModel[]
     */
    protected $model;
    /**
     * @var array of viewable fields in the grid view
     */
    protected $fields = [];

    /**
     * @param iModel $model
     * @param iModel[] $models
     * @param array $fields
     * @param array $options
     * @return $this
     */
    public static function begin(iModel $model, array $models, array $fields = [], array $options = [])
    {
        $self = static::getInstance();

        if (empty($models) && empty($fields))
            return $self;

        reset($models);

        if (empty($fields)) {
            $model = current($models);
            $fields = $model->fields();
        }

        $self->fields = $fields;
        $self->model = $models;

        return $self->build($model, $models, $options);
    }

    /**
     * Render a complete html table by a 2d array of rows and cells.
     * @param iModel $model
     * @param iModel[] $models
     * @param array $options of table [has_header, table, rows, cells, header]
     * @return $this
     */
    protected function build(iModel $model, array $models, array $options = [])
    {
        $body = '';
        $field_labels = $model->getFieldLabels($this->fields);
        $this->table = Table::begin(array_get($options, 'table', []));
        $tableHasHeader = array_get($options, 'has_header', true);
        $tableHasAction = array_get($options, 'has_action', true);
        $bodyOptions = array_get($options, 'tbody', []);
        $rowOptions = array_get($options, 'rows', []);
        $cellOptions = array_get($options, 'cells', []);

        foreach ($models as $model) {
            $row = [];

            foreach ($this->fields as $field) {
                if (is_array($field)) {
                    if ($_value = array_get($field, 'value')) {
                        $value = $_value;
                    } else {
                        $value = $this->getValue($model, $field['field']);
                    }
                } else {
                    $value = $this->getValue($model, $field);
                }

                $row[] = $value;
            }

            if ($tableHasAction === true) {
                $buttons = [];
                $link = new Link();
                $id = $model->{$model::primaryKey()};
                $controllerRoute = container()->Template->controller->getControllerRoute();
                $_tableAction_params = array_get($options, 'action_params', ['id' => $id]);
                $tableAction_params = '';

                foreach ($_tableAction_params as $param_key => $param_value) {
                    $clean_key_count = 1;

                    if (strpos($param_key, 'field:') === 0) {
                        if (strstr($param_value, '.')) {
                            $sub_model = explode('.', $param_value);
                            $param_value = $model;

                            foreach ($sub_model as $item)
                                $param_value = $param_value->{$item};
                        } else {
                            $param_value = $model->{$param_value};
                        }
                    }

                    $tableAction_params .= str_replace('field:', '', $param_key, $clean_key_count) . '=' . $param_value . '&';
                }

                $tableAction_params = rtrim($tableAction_params, '&');

                $buttons[] = $link->begin([
                    'caption' => 'View',
                    'href' => urlTo($controllerRoute . '/view?' . $tableAction_params)
                ])->render(true);

                $buttons[] = $link->begin([
                    'caption' => 'Edit',
                    'href' => urlTo($controllerRoute . '/edit?' . $tableAction_params)
                ])->render(true);

                $buttons[] = $link->begin([
                    'caption' => 'Delete',
                    'href' => urlTo($controllerRoute . '/delete?' . $tableAction_params),
                    'class' => 'text-danger action-btn action-delete'
                ])->render(true);

                $row[] = implode(' | ', $buttons);
            }

            $body .= $this->table->addRow($row, 'td', $rowOptions, $cellOptions)->render(true);
        }

        if ($tableHasHeader === true) {
            $headerOptions = array_get($options, 'header', []);

            if ($tableHasAction === true) {
                $field_labels[] = 'Actions';
            }

            $this->table->addHeader($field_labels, $headerOptions);
        }

        $this->table->addBody($body, $bodyOptions);

        return $this;
    } // End table func

    protected function getValue($model, $field)
    {
        if (strstr($field, '.')) {
            $value = $model;
            $_sub_field = explode('.', $field);

            foreach ($_sub_field as $sub_field) {
                if (!is_object($value))
                    break;

                $value = $value->{$sub_field};
            }
        } else {
            $value = $model->{$field};
        }

        if (is_null($value))
            $value = '<em class="small">(not set)</em>';

        return $value;
    }

    public function render($return = false)
    {
        if (is_null($this->table))
            return false;

        return $this->table->render($return);
    }
}