<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/8/2018
 * Time: 2:52 PM
 */

namespace core\helpers\html\form;


use core\helpers\html\BaseElement;

class Form extends BaseElement
{
    public $method = 'POST';

    protected $tag = 'form';
    protected $hasEnd = true;
    protected $methods = ['GET', 'POST'];
    protected $escapeEmptyAttributes = ['enctype', 'id'];

    public function render($return = false)
    {
        $this->attributes['method'] = $this->getMethod();

        foreach ($this->escapeEmptyAttributes as $attribute) {
            if (isset($this->attributes[$attribute]) && empty($this->attributes[$attribute]))
                unset($this->attributes[$attribute]);
        }

        $this->buildElement();

        if ($return === true)
            return $this->output;

        echo $this->output;
        return $this;
    }

    protected function getMethod()
    {
        $method = strtoupper($this->method);

        return (!in_array($method, $this->methods)) ? 'POST' : $method;
    }
}