<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/8/2018
 * Time: 3:16 PM
 */

namespace core\helpers\html\form;


use core\helpers\html\BaseElement;

class Label extends BaseElement
{
    protected $tag = 'label';
    protected $hasEnd = true;

    public function render($return = false)
    {
        if ($label = array_get($this->attributes, 'label', false)) {
            unset($this->attributes['label']);
            $this->append($label);
        }

        $this->buildElement();

        if ($return === true)
            return $this->output;

        echo $this->output;

        return $this;
    }
}