<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/11/2018
 * Time: 3:22 PM
 */

namespace core\helpers\html\form;


use core\helpers\html\BaseElement;

class Button extends BaseElement
{
    public $type = 'button';
    public $caption = 'Button';

    protected $tag = 'button';
    protected $hasEnd = true;
    protected $types = ['button', 'submit', 'reset'];

    public function render($return = false)
    {
        $this->attributes['type'] = $this->getType();

        $this->append($this->caption);

        $this->buildElement();

        if ($return === true)
            return $this->output;

        echo $this->output;

        return $this;
    }

    protected function getType()
    {
        if (!in_array($this->type, $this->types))
            $this->type = 'button';
    }
}