<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/17/2018
 * Time: 5:58 PM
 */

namespace core\helpers\html\form;


use core\helpers\html\BaseElement;

class SelectBox extends BaseElement
{
    public $items = [];

    public $optionTemplate = '<option value="{value}"{selected}>{label}</option>';

    protected $tag = 'select';
    protected $hasEnd = true;

    public function items(array $items)
    {
        $this->items = $items;
    }

    public function render($return = false)
    {
        if (!empty($this->items)) {
            foreach ($this->items as $value => $label) {
                $fieldParams = [$value, $label, ''];

                if (array_get($this->attributes, 'value', $this->value) == $value) {
                    $fieldParams[2] = ' selected';
                }

                $option = str_replace(['{value}', '{label}', '{selected}'], $fieldParams, $this->optionTemplate);
                $this->append($option);
            }
        }

        $this->buildElement();

        if ($return === true)
            return $this->output;

        echo $this->output;

        return $this;
    }
}