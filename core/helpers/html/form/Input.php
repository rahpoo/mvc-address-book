<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/7/2018
 * Time: 3:33 PM
 */

namespace core\helpers\html\form;


use core\helpers\html\BaseElement;

class Input extends BaseElement
{
    public $type = 'text';

    protected $tag = 'input';
    protected $hasEnd = false;
    protected $types = ['text', 'password', 'number', 'file', 'submit', 'reset'];

    public function render($return = false)
    {
        $this->attributes['type'] = $this->getType();

        $this->buildElement();

        if ($return === true)
            return $this->output;

        echo $this->output;

        return $this;
    }

    protected function getType()
    {
        return (!in_array($this->type, $this->types)) ? 'text' : $this->type;
    }
}