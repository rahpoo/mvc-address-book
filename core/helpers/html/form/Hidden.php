<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/7/2018
 * Time: 3:33 PM
 */

namespace core\helpers\html\form;


use core\helpers\html\BaseElement;

class Hidden extends BaseElement
{
    public $hasLabel = false;

    protected $tag = 'input';
    protected $hasEnd = false;

    public function render($return = false)
    {
        $this->attributes['type'] = 'hidden';

        $this->buildElement();

        if ($return === true)
            return $this->output;

        echo $this->output;

        return $this;
    }
}