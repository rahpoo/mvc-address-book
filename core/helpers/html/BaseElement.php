<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/7/2018
 * Time: 2:22 AM
 */

namespace core\helpers\html;


use core\base\Component;
use core\contracts\iRenderable;

class BaseElement extends Component implements iRenderable
{
    public $hasLabel = true;

    /**
     * @var string
     */
    protected $tag;
    /**
     * @var bool determine the html tag has twice tag or not
     */
    protected $hasEnd = true;
    /**
     * @var array|string
     */
    protected $attributes = [];
    /**
     * @var string
     */
    protected $output = '';

    public static function getInstance(array $options = [])
    {
        static::$instance = new static($options);

        foreach ($options as $name => $value) {
            if (isset(static::$instance->{$name}))
                unset($options[$name]);
        }

        static::$instance->attributes = $options;

        return static::$instance;
    }

    /**
     * Set tag name.
     * @param string $tag_name
     * @return $this
     */
    public function setTag($tag_name = "")
    {
        if (!empty($tag_name))
            $this->tag = $tag_name;

        return $this;
    }

    /**
     * Add attributes to current element.
     * @param array|string $options
     * @return $this
     */
    public function options($options = [])
    {
        $this->attributes = array_merge($this->attributes, $options);

        return $this;
    }

    /**
     * @param null|array|string $options
     * @return $this
     */
    public static function begin($options = array())
    {
        $self = static::getInstance($options);
        $self->value = '';

        return $self;
    }

    public function end()
    {
        if (!$this->hasEnd)
            $this->output = str_replace('>', ' />', $this->output);
        elseif (!strstr($this->output, '</' . $this->tag . '>'))
            $this->output .= '</' . $this->tag . '>';

        return $this;
    }

    /**
     * @param string|\core\contracts\iRenderable $input
     * @return $this|string if auto_append is false, output will be string
     */
    public function append($input)
    {
        if (is_a($input, '\core\contracts\iRenderable'))
            $this->output .= $input->render(true);
        else
            $this->output .= $input;

        return $this;
    }

    public function render($return = false)
    {
        $this->buildElement();

        if ($return === true)
            return $this->output;

        echo $this->output;
        return $this;
    }

    protected function buildElement()
    {
        $output = '<' . $this->tag . $this->parseAttributes() . '>';
        $this->output = $output . $this->output;
        $this->end();
    }

    /**
     * Parse html attribute from array of options.
     * @return string
     */
    protected function parseAttributes()
    {
        $output = '';

        if (!empty($this->attributes)) {
            if (is_string($this->attributes)) {
                $output .= ' ' . $this->attributes;
            } elseif (is_array($this->attributes)) {
                foreach ($this->attributes as $option_key => $option_val)
                    $output .= ' ' . $option_key . '="' . $option_val . '"';
            }
        }

        return $output;
    }
}