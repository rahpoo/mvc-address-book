<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/7/2018
 * Time: 3:05 PM
 */

namespace core\helpers\html;


class Link extends BaseElement
{
    public $caption = '';

    protected $tag = 'a';
    protected $hasEnd = true;

    public function render($return = false)
    {
        if (!empty($this->caption))
            $this->append($this->caption);

        $this->buildElement();

        if ($return === true)
            return $this->output;

        echo $this->output;

        return $this;
    }
}