<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/23/2018
 * Time: 10:26 PM
 */

namespace core\helpers\html\table;


use core\helpers\html\BaseElement;

class Table extends BaseElement
{
    protected $tag = 'table';
    protected $hasEnd = true;
    protected $cellTypes = [
        'th' => '\core\helpers\html\table\HeadCell',
        'td' => '\core\helpers\html\table\Cell'
    ];

    /**
     * Add a `thead` tag to the table.
     * @param $cells
     * @param array $options
     */
    public function addHeader($cells, $options = [])
    {
        $rowOptions = array_get($options, 'rows', []);
        $cellOptions = array_get($options, 'cells', []);

        unset($options['rows'], $options['cells']);

        $headRow = $this->addRow($cells, 'th', $rowOptions, $cellOptions);
        $this->append((new Head())->begin($options)->append($headRow)->end());
    }

    public function addBody($content, $options = [])
    {
        $body = Body::begin($options);

        if (is_array($content)) {
            foreach ($content as $row) {
                $body->append($row);
            }
        } else {
            $body->append($content);
        }

        $this->append($body);

        return $this;
    }

    /**
     * Render and return a complete row with cells.
     * @param array $cells
     * @param string $cell_type
     * @param array|string $rowOptions
     * @param array|string $cellOptions
     * @return string|$this
     */
    public function addRow(array $cells = [], $cell_type = "td", $rowOptions = [], $cellOptions = [])
    {
        $row = Row::begin($rowOptions);
        $cell_type = array_get($this->cellTypes, $cell_type, array_get($this->cellTypes, 'td'));

        foreach ($cells as $index => $cell) {
            if (is_array($cell) || is_object($cell) || is_resource($cell))
                $cell = '';

            $row->append($cell_type::begin($cellOptions)->append($cell));
        }

        return $row;
    }
}