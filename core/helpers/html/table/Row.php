<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/23/2018
 * Time: 10:28 PM
 */

namespace core\helpers\html\table;


use core\helpers\html\BaseElement;

class Row extends BaseElement
{
    protected $tag = 'tr';
    protected $hasEnd = true;
}