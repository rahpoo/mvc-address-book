<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/23/2018
 * Time: 10:37 PM
 */

namespace core\helpers\html\table;


use core\helpers\html\BaseElement;

class Body extends BaseElement
{
    protected $tag = 'tbody';
    protected $hasEnd = true;
}