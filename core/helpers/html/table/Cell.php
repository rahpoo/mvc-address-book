<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/23/2018
 * Time: 10:29 PM
 */

namespace core\helpers\html\table;


use core\helpers\html\BaseElement;

class Cell extends BaseElement
{
    protected $tag = 'td';
    protected $hasEnd = true;
}