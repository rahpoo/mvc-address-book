<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/23/2018
 * Time: 10:39 PM
 */

namespace core\helpers\html\table;


use core\helpers\html\BaseElement;

class Head extends BaseElement
{
    protected $tag = 'thead';
    protected $hasEnd = true;
}