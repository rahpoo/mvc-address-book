<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 7/5/2018
 * Time: 7:38 PM
 */

namespace core\helpers;

/**
 * Class FileHelper
 * @package helpers
 */
class FileHelper
{
    protected $file_path = '';

    /**
     * FileHelper constructor.
     * @param string $file_path
     */
    public function __construct($file_path = "")
    {
        $this->setFilePath($file_path);
    }

    /**
     * @param string $file_path
     * @return $this
     */
    public function setFilePath($file_path)
    {
        if (!is_string($file_path))
            throw new \InvalidArgumentException('Parameter $file_path must be instance of string!');

        $this->file_path = $file_path;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->file_path;
    }

    /**
     * Check file exists and if true return real path of file else return false.
     * @param $file_path
     * @return bool|string
     */
    public function getFileRealPath($file_path = null)
    {
        $file_path = $file_path ?? $this->file_path;

        if (empty($file_path) || !is_string($file_path))
            return false;

        $file_path = realpath($file_path);
        if (!file_exists($file_path))
            return false;
        return $file_path;
    }

    /**
     * Get contents of a file if exists and return string.
     * If there is any problem it's will return false.
     * @return bool|string
     */
    public function getFileContents()
    {
        return file_get_contents($this->getFileRealPath($this->getFilePath()));
    }

    public function setFileContents($data)
    {
        return file_put_contents(realpath($this->file_path), $data);
    }
}