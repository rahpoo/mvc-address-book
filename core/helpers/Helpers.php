<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 7/5/2018
 * Time: 12:12 AM
 */

if (!function_exists('dd')) {
    /**
     * var_dump and die.
     */
    function dd()
    {
        $args = func_get_args();
        call_user_func_array('var_dump', $args);
        die();
    }
}

if (!function_exists('pd')) {
    /**
     * print_r and die.
     * @param array $args
     */
    function pd(array $args)
    {
        print_r($args);
        die();
    }
}

if (!function_exists('container')) {
    /**
     * @return \core\Container
     */
    function container()
    {
        global $Container;
        return $Container;
    }
}

if (!function_exists('getDB')) {
    /**
     * @return \core\contracts\iDBConnection
     */
    function getDB()
    {
        return container()->get('DB');
    }
}

if (!function_exists('getRouter')) {
    /**
     * @return \core\base\Router
     */
    function getRouter()
    {
        return container()->get('Router');
    }
}

if (!function_exists('array_first_key')) {
    /**
     * Get first key of an array.
     * @param $array
     * @return int|null|string
     */
    function array_first_key($array)
    {
        reset($array);
        return key($array);
    }
}

if (!function_exists('array_first')) {
    /**
     * Get first element of and array.
     * @param $array
     * @return mixed
     */
    function array_first($array)
    {
        reset($array);
        return $array[key($array)];
    }
}

if (!function_exists('array_get')) {
    /**
     * Safe get a value of an array.
     * @param array $arr
     * @param mixed $key
     * @param null $default
     * @return mixed|null
     */
    function array_get(array $arr, $key, $default = null)
    {
        return (isset($arr[$key])) ? $arr[$key] : $default;
    }
}

if (!function_exists('isPost')) {
    function isPost()
    {
        return ($_SERVER['REQUEST_METHOD'] === 'POST');
    }
}

if (!function_exists('isPost')) {
    /**
     * @return bool true if request method is POST
     */
    function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }
}

if (!function_exists('isGet')) {
    /**
     * @return bool true if request method is GET
     */
    function isGet()
    {
        return $_SERVER['REQUEST_METHOD'] === 'GET';
    }
}

if (!function_exists('request')) {
    /**
     * Safe get a value of $_REQUEST.
     *
     * @param mixed $key
     * @param null $default
     * @param string $type string|int|float|double|bool|array|object|serialize|unserialize|json|unjson
     * @param boolean $pop unset index if its true
     * @return mixed|null
     */
    function request($key = '', $default = null, $type = 'string', $pop = false)
    {
        if (empty($key))
            return $_REQUEST;

        $output = array_get($_REQUEST, $key, $default);
        typecast($output, $type);

        if ($pop)
            unset($_REQUEST[$key]);

        return $output;
    }
}

if (!function_exists('get')) {
    /**
     * Safe get a value of $_GET.
     *
     * @param mixed $key
     * @param null $default
     * @param string $type string|int|float|double|bool|array|object|serialize|unserialize|json|unjson
     * @param boolean $pop unset index if its true
     * @return mixed|null
     */
    function get($key = '', $default = null, $type = 'string', $pop = false)
    {
        if (empty($key))
            return $_GET;

        $output = array_get($_GET, $key, $default);
        typecast($output, $type);

        if ($pop && !empty($key))
            unset($_GET[$key]);

        return $output;
    }
}

if (!function_exists('post')) {
    /**
     * Safe get a value of $_POST.
     *
     * @param mixed $key
     * @param null $default
     * @param string $type string|int|float|double|bool|array|object|serialize|unserialize|json|unjson
     * @param boolean $pop unset index if its true
     * @return mixed|null
     */
    function post($key = '', $default = null, $type = 'string', $pop = false)
    {
        if (empty($key))
            return $_POST;

        $output = array_get($_POST, $key, $default);
        typecast($output, $type);

        if ($pop)
            unset($_POST[$key]);

        return $output;
    }
}

if (!function_exists('typecast')) {
    /**
     * Casts of variable types.
     *
     * @param mixed $var by reference
     * @param string $type string|int|float|double|bool|array|object|serialize|unserialize|json|unjson
     */
    function typecast(&$var, $type = 'string')
    {
        switch ($type) {
            case 'string':
            default:
                settype($var, 'string');
                break;

            case 'int':
            case 'integer':
                settype($var, 'integer');
            break;

            case 'float':
                settype($var, 'float');
            break;

            case 'double':
                settype($var, 'double');
            break;

            case 'bool':
            case 'boolean':
                settype($var, 'boolean');
                break;

            case 'array':
                settype($var, 'array');
                break;

            case 'object':
                settype($var, 'object');
                break;

            case 'serialize':
                $var = serialize($var);
                break;
            case 'unserialize':
                $var = unserialize($var);
                break;

            case 'json':
                $var = json_encode($var);
                break;
            case 'unjson':
                $var = json_decode($var);
                break;
        }
    }
}

if (!function_exists('sanitize_router')) {
    /**
     * @param $string
     * @param bool $capitalizeFirstCharacter
     * @return mixed|string
     */
    function sanitize_router($string, $capitalizeFirstCharacter = false)
    {
        $string = str_replace('-', '', ucwords($string, '-'));
        return (!$capitalizeFirstCharacter) ? lcfirst($string) : $string;
    }
}

if (!function_exists('camel_to_dashed')) {
    /**
     * @param $input
     * @param string $delimiter
     * @return string
     */
    function camel_to_dashed($input, $delimiter = '-')
    {
        return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1'.$delimiter, $input));
    }
}

if (!function_exists('underline_to_dash')) {
    /**
     * @param $input
     * @return string
     */
    function underline_to_dash($input)
    {
        return strtolower(preg_replace('/_/', '$1-', $input));
    }
}

if (!function_exists('camel_to_word')) {
    /**
     * @param $name
     * @param bool $ucwords
     * @return mixed|string
     */
    function camel_to_word($name, $ucwords = true)
    {
        $label = str_replace(['-', '_', '.'], ' ', preg_replace('/(?<![A-Z])[A-Z]/', ' \0', strtolower(trim($name))));
        return $ucwords ? ucwords($label) : $label;
    }
}

if (!function_exists('baseUrl')) {
    function baseUrl(){
        return sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            str_replace('index.php', '', $_SERVER['PHP_SELF'])
        );
    }
}

if (!function_exists('urlTo')) {
    function urlTo($path){
        return baseUrl() . $path;
    }
}

if (!function_exists('redirect_not_found')) {
    function redirect_not_found($msg = '') {
        redirect();
    }
}

if (!function_exists('redirect')) {
    function redirect($path) {
        header("Location: $path");
        die();
    }
}

if (!function_exists('array_sort')) {
    /**
     * Multiple sort an array by column.
     * @param array $arr
     * @param int|string $col
     * @param int $sort_flag
     * @return bool
     */
    function array_sort(array &$arr, $col, $sort_flag = SORT_ASC)
    {
        return usort($arr, function ($a, $b) use ($col, $sort_flag) {
            if (!isset($a[$col]))
                return false;

            $a_val = trim($a[$col]);
            $b_val = trim($b[$col]);

            // Specified for price (with symbol) sorting
            $a_val = trim($a_val, '($|€|£|ƒ|¥|₡|£|₪| )');
            if (preg_match('~^\\d+(?:\\.\\d{1,2})?$~', $a_val)) {
                $a_val = filter_var( str_replace(",", "", $a_val), FILTER_SANITIZE_NUMBER_FLOAT);
                $b_val = filter_var( str_replace(",", "", $b_val), FILTER_SANITIZE_NUMBER_FLOAT);
            }

            if ($sort_flag == SORT_ASC) {
                return ($a_val <= $b_val) ? -1 : 1;
            } else {
                return ($a_val >= $b_val) ? -1 : 1;
            }
        });
    }
}

if (!function_exists('array_flat')) {
    /**
     * Flatten a multi-level array.
     * @param array $array
     * @return array
     */
    function array_flat(array $array)
    {
        $output = [];
        $array = new RecursiveIteratorIterator(new RecursiveArrayIterator($array));

        foreach($array as $value) {
            $output[] = $value;
        }

        return $output;
    }
}
