<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/30/2018
 * Time: 12:47 AM
 */

namespace core\contracts;


interface iDBConnection
{
    public function connect();
    public function disconnect();
    public function getConnection();
}