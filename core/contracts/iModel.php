<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/30/2018
 * Time: 12:55 AM
 */

namespace core\contracts;

/**
 * Interface iModel
 * @package core\contracts
 *
 * @property array $attributes
 */
interface iModel
{
    public static function tableName();
    public static function primaryKey();
    public static function validation();
    public static function find() : iQueryBuilder;
    public function save();
    public function isNewRecord();
    public function fields();
    public function labels();
    public function getFieldLabels(array $fields = []);
    public function getFieldLabel($field);
    public function load(array $data);
    public function validate($fields = null);
    public function addError($field, $error = null);
    public function addErrors($errors=[]);
    public function getErrors();
}