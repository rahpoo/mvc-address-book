<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 11:03 PM
 */

namespace core\contracts;


interface iComponent
{
    public function __construct(array $configs = []);
    public function init();
}