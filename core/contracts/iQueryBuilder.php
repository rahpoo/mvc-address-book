<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/30/2018
 * Time: 2:05 AM
 */

namespace core\contracts;


interface iQueryBuilder
{
    public function where($field, $operator = '', $value = '') : iQueryBuilder;
    public function select($columns) : iQueryBuilder;
    public static function find($condition = []) : iQueryBuilder;
    public function orderBy($fields = []) : iQueryBuilder;
    public function groupBy($fields = '') : iQueryBuilder;
    public function asArray() : iQueryBuilder;
    public function one();
    public function all();
    public function insert($columns = []);
    public function update($columns = []);
    public function delete($conditions = []);
    public function createCommand();
    public function getQuery();
    public function getQueryString();
}