<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/7/2018
 * Time: 2:36 AM
 */

namespace core\contracts;


interface iRenderable
{
    public function render($return = false);
}