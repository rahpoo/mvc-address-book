<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/29/2018
 * Time: 1:20 AM
 */

namespace core\contracts;


use core\base\Router;

interface iController
{
    public function __construct(Router $router, string $controller, string $action);
    public function init();

    public function beforeAction() : bool;
    public function afterAction();
    public function run();
}