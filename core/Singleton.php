<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 12:15 AM
 */

namespace core;

/**
 * Class Singleton.
 * @package core
 */
class Singleton
{
    /**
     * Instances of classes.
     * @var static
     */
    private static $instances = [];

    /**
     * @return string name of current class.
     */
    public static function className()
    {
        return get_called_class();
    }

    /**
     * Call this method to get a singleton instance.
     * @return static
     */
    public static function getInstance()
    {
        $class = static::className();

        if (!isset(self::$instances[$class]))
            self::$instances[$class] = new $class();

        return self::$instances[$class];
    }

    /**
     * Private __construct so nobody else can instantiate it.
     */
    private function __construct()
    {
    }

    /**
     * Private __clone so nobody else can clone it.
     */
    private function __clone()
    {
    }
}