<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 12:26 AM
 */

namespace core\base;


use core\contracts\iComponent;
use core\exceptions\ReadOnlyProperty;
use core\exceptions\UnknownProperty;

/**
 * Class Component.
 * This class is base of other modules or application classes.
 * @package core\base
 */
class Component implements iComponent
{
    protected static $instance;

    /**
     * @var array of dynamic properties of class
     */
    private $vars = [];

    public function __construct(array $configs = [])
    {
        $this->configure($configs);
        $this->init();

        return $this;
    }

    public static function getInstance(array $configs = [])
    {
        if (!is_null(static::$instance))
            return static::$instance;

        static::$instance = new static($configs);
        return static::$instance;
    }

    public function init()
    {

    }

    public function className()
    {
        return get_called_class();
    }

    protected function configure(array $configs = [])
    {
        foreach ($configs as $name => $value) {
            $this->{$name} = $value;
        }
    }

    /**
     * @param $name
     * @param $value
     * @throws ReadOnlyProperty
     */
    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        } elseif (property_exists($this, $name)) {
            try {
                $this->{$name} = $value;
            } catch (\Exception $e) {
                throw new ReadOnlyProperty($this::className(), $name);
            }
        } elseif (method_exists($this, 'get' . $name)) {
            throw new ReadOnlyProperty($this::className(), $name);
        } else {
            $this->vars[$name] = $value;
        }
    }

    /**
     * @param $name
     * @return mixed
     * @throws UnknownProperty
     */
    public function __get($name)
    {
        $getter = 'get' . ucfirst($name);
        if (method_exists($this, $getter))
            return $this->$getter($name);

        elseif (property_exists($this, $name))
            return $this->{$name};

        elseif (isset($this->vars[$name]))
            return $this->vars[$name];

        throw new UnknownProperty($this::className(), $name);
    }



    public function __destruct()
    {

    }
}