<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 5:56 PM
 */

namespace core\base;

/**
 * Class Router.
 * This class will do routing to load requested controller and run its action.
 *
 * @package core\base
 */
class Router extends Component
{
    public $defaultController = 'default';
    public $defaultAction = 'index';
    public $controllerPath;
    public $controllerNamespace;

    protected $controller;
    protected $action;

    /**
     * Router initializer.
     * @throws \Exception
     */
    public function init()
    {
        $this->validateControllerPath();
        $this->lunch();
    }

    /**
     * @throws \Exception
     */
    protected function validateControllerPath()
    {
        if (empty($this->controllerPath)) {
            $this->controllerPath = 'app/controllers';
            $this->controllerNamespace = '\\app\\controllers\\';
        }

        if (!is_dir($this->controllerPath)) {
            throw new \Exception("Invalid controller path `$this->controllerPath`");
        }

        if (is_null($this->controllerNamespace))
            $this->controllerNamespace = str_replace('/', '\\', $this->controllerPath). '\\';

        $this->controllerPath .= DS;
    }

    /**
     * @throws \Exception
     */
    public function lunch()
    {
        $route = get('page', $this->defaultController, 'string', true);
        $routeParts = explode('/', $route); // Limit to controller/action but it is possible to add some url route pattern

        $_controller = array_get($routeParts, 0);
        $this->controller = sanitize_router($_controller, true);
        $controller = $this->controllerNamespace . $this->controller . 'Controller';
        if ($_controller != 'page-not-found' && !class_exists($controller)) {
            redirect(urlTo('page-not-found'));
        }
        $this->action = sanitize_router(array_get($routeParts, 1, $this->defaultAction));

        if (!class_exists($controller)) {
            throw new \Exception("Controller `$controller` was not found!");
        }

        $controller = call_user_func([new \ReflectionClass($controller), 'newInstance'], $this, $this->controller, $this->action);

        if (!is_a($controller, '\core\contracts\iController')) {
            throw new \Exception("The controller must be instance of `\core\base\Controller`");
        }
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getAction()
    {
        return $this->action;
    }
}