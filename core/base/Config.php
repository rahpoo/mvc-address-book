<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 12:24 AM
 */

namespace core\base;


use core\helpers\FileHelper;

class Config extends Component
{
    public $configPath;
    public $configs = [];

    public function init()
    {
        if (is_null($this->configPath)) {
            $this->configPath = ROOT . 'app' . DS . 'config' . DS;
        }

        $files = array_slice(scandir($this->configPath), 2);

        if (!empty($files)) {
            foreach ($files as $file) {
                $fileHelper = new FileHelper($this->configPath . $file);
                if ($file_path = $fileHelper->getFileRealPath()) {
                    $configs = require_once $file_path;
                    if (is_array($configs) && !empty($configs))
                        $this->configs = array_merge($this->configs, $configs);
                }
            }
        }
    }

    public function get($name, $default = null)
    {
        return array_get($this->configs, $name, $default);
    }

}