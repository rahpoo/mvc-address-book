<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 8:44 PM
 */

namespace core\base;


use core\exceptions\InvalidViewFile;
use core\helpers\FileHelper;

/**
 * Class Template
 * @package app\base
 *
 * @property string $resource_path
 * @property string $layout_dir
 * @property string $view_dir
 * @property string $layout
 */
class Template extends Component
{
    /**
     * @var FileHelper
     */
    protected $view_file;
    /**
     * @var array
     */
    protected $vars = array();

    public $resource_path = 'app' . DS . 'resources' . DS;
    public $layout_dir = 'layouts' . DS;
    public $view_dir = 'views' . DS;
    public $layout = 'main';
    /**
     * @var Controller
     */
    public $controller;

    public function __construct(array $configs = [])
    {
        $configs['layout'] = array_get($configs, 'layout', $this->layout);

        parent::__construct($configs);
    }

    /**
     * @param $view
     * @param array $params
     * @throws InvalidViewFile
     */
    public function view($view, $params = array())
    {
        $layoutFilePath = $this->validateLayoutFile();
        $viewFilePath = $this->validateViewFile($view);
        $this->vars = array_merge($this->vars, (array) $params);

        foreach ($this->vars as $key => $value)
            $$key = $value;

        ob_start();
        include $viewFilePath;
        $content = ob_get_clean();

        include $layoutFilePath;
    }

    /**
     * @return bool|string
     * @throws InvalidViewFile
     */
    public function validateLayoutFile()
    {
        $layoutFile = $this->getLayoutPath($this->layout);
        $this->view_file = new FileHelper($layoutFile);
        $viewFilePath = $this->view_file->getFileRealPath($layoutFile);
        if ($viewFilePath === false)
            throw new InvalidViewFile('Layout View "' . $this->layout . '" was not found in the following path: ' . $this->view_file->getFilePath());
        else return $viewFilePath;
    }

    /**
     * @param string $view
     * @return bool|string
     * @throws InvalidViewFile
     */
    public function validateViewFile($view)
    {
        if (substr($view, 0, 1) != '/') {
            $view = camel_to_dashed($this->controller->getControllerName()) . '/' . $view;
        }

        $viewFilePath = $this->getViewPath($view);
        $this->view_file = new FileHelper($viewFilePath);
        $viewFilePath = $this->view_file->getFileRealPath($viewFilePath);
        if ($viewFilePath === false)
            throw new InvalidViewFile('View "' . $view . '" was not found in the following path: ' . $this->view_file->getFilePath());
        else return $viewFilePath;
    }

    /**
     * @param $layout
     * @return string
     */
    public function getLayoutPath($layout)
    {
        return $this->resource_path . $this->layout_dir . $layout . '.php';
    }

    /**
     * @param $view
     * @return string
     */
    public function getViewPath($view)
    {
        return $this->resource_path . $this->view_dir . $view . '.php';
    }

}