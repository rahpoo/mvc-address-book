<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/9/2018
 * Time: 1:50 AM
 */

namespace core\base;


use core\contracts\iRenderable;

abstract class Widget extends Component implements iRenderable
{
    protected $output;

    abstract public function render($return = false);
}