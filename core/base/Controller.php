<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 5:56 PM
 */

namespace core\base;


use core\contracts\iController;

/**
 * Class Controller is the base controller.
 * @package core\base
 */
class Controller implements iController
{
    /**
     * @var string of current controller extends of base\controller
     */
    public $controller;
    /**
     * @var string name of action method in current controller
     */
    public $action;
    /**
     * @var Template object
     */
    public $template;
    /**
     * @var string name of layout view
     */
    public $layout;

    protected $router;
    protected $controllerName;
    protected $controllerRoute;

    /**
     * Controller constructor.
     * @param Router $router
     * @param string $controller
     * @param string $action
     * @throws \Exception
     */
    public function __construct(Router $router, string $controller, string $action)
    {
        $this->router = $router;
        $this->controllerRoute = camel_to_dashed($controller);
        $this->controllerName = $controller;
        $this->controller = static::class;
        $this->action = $action . 'Action';

        if (!method_exists($this->controller, $this->action))
            redirect(urlTo('page-not-found'));

        container()->set('Template', '\core\base\Template', ['layout' => $this->layout, 'controller' => $this]);

        $this->template = container()->Template;
        $this->init();
        $this->run();
    }

    public function init()
    {

    }

    public function getControllerName()
    {
        return $this->controllerName;
    }

    public function getControllerRoute()
    {
        return $this->controllerRoute;
    }

    /**
     * Doing something or validate something before run action method.
     * @return bool
     */
    public function beforeAction(): bool
    {
        return true;
    }

    /**
     * Doing something after run action method.
     */
    public function afterAction()
    {

    }

    /**
     * Run action method if beforeRun() method returns true.
     */
    public function run()
    {
        if ($this->beforeAction()) {
            $output = call_user_func_array([$this, $this->action], get());

            if (is_string($output)) {
                echo $output;
            } elseif (is_array($output) || is_object($output)) {
                echo json_encode((array)$output);
            }

            $this->afterAction();
        }
    }

    /**
     * @param $view
     * @param array $vars
     * @throws \core\exceptions\InvalidViewFile
     */
    public function render($view, $vars = [])
    {
        $this->template->controller = $this;
        $this->template->view($view, $vars);
    }
}