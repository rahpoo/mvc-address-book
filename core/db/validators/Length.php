<?php
/**
 * Created by PhpStorm.
 * User: 98919
 * Date: 11/24/2018
 * Time: 1:56 PM
 */

namespace core\db\validators;


class Length extends Validator
{
    public function validate()
    {
        $model = $this->model;

        foreach ($this->fields as $field) {
            $length = mb_strlen($model->{$field});

            if (count($this->params) == 1) {
                if ($this->params[0] < $length)
                    $this->addError(
                        $field,
                        "length must be less '{$this->params[0]}' characters"
                    );
            } elseif (($this->params[0] > 0 && $length < $this->params[0]) || $length > $this->params[1]) {
                $this->addError(
                    $field,
                    "length must be between '{$this->params[0]}' and '{$this->params[1]}' characters"
                );
            } elseif ($this->params[0] == 0 && $length > $this->params[1]) {
                $this->addError(
                    $field,
                    "length must be less '{$this->params[1]}' characters"
                );
            }
        }
    }
}