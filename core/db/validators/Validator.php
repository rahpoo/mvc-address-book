<?php
/**
 * Created by PhpStorm.
 * User: 98919
 * Date: 11/24/2018
 * Time: 12:38 PM
 */

namespace core\db\validators;


use core\contracts\iModel;

abstract class Validator implements iValidator
{
    /**
     * @var iModel
     */
    protected $model;
    /**
     * @var array of field to validate
     */
    protected $fields = [];
    /**
     * @var array of required parameters for validator
     */
    protected $params = [];

    abstract public function validate();

    public function __construct(iModel &$model, $fields, $params = [])
    {
        $this->model = $model;
        $this->fields = (array) $fields;
        $this->params = (array) $params;
    }

    protected function addError($field = false, $message = '')
    {
        $this->model->addError($field, $message);
    }
}