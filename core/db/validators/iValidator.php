<?php
/**
 * Created by PhpStorm.
 * User: 98919
 * Date: 11/24/2018
 * Time: 12:29 PM
 */

namespace core\db\validators;


use core\contracts\iModel;

interface iValidator
{
    public function validate();
}