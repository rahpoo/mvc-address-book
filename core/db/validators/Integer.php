<?php
/**
 * Created by PhpStorm.
 * User: 98919
 * Date: 11/24/2018
 * Time: 2:30 PM
 */

namespace core\db\validators;


class Integer extends Validator
{
    public $integerPattern = '/^\s*[-+]?[0-9]*\s*$/';

    public function validate()
    {
        $model = $this->model;

        foreach ($this->fields as $field) {
            if (!preg_match($this->integerPattern, $model->{$field})) {
                $this->addError($field, 'must be an integer value!');
            }
        }
    }
}