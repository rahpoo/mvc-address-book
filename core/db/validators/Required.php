<?php
/**
 * Created by PhpStorm.
 * User: 98919
 * Date: 11/24/2018
 * Time: 12:33 PM
 */

namespace core\db\validators;


/**
 * Class Required
 * @package core\db\validators
 *
 * @example [['firstname', 'lastname', 'cityid', 'street'], 'required']
 */
class Required extends Validator
{
    public function validate()
    {
        $model = $this->model;

        foreach ($this->fields as $field) {
            if (is_null($model->{$field}) || $model->{$field} == '')
                $this->addError($field, 'is required!');
        }
    }
}