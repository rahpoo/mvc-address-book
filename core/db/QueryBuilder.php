<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/30/2018
 * Time: 12:40 AM
 */

namespace core\db;


use core\base\Component;
use core\contracts\iQueryBuilder;
use core\exceptions\ReadOnlyProperty;
use core\exceptions\UnknownProperty;

/**
 * Class QueryBuilder.
 * This component is base of other database QueryBuilders and their Models.
 * @package core\db
 */
abstract class QueryBuilder extends Component implements iQueryBuilder
{
    public $isNewRecord = false;
    /**
     * @var array consist of fields and their values of database table
     */
    public $attributes = [];
    /**
     * @var array consist of old field values of database table
     */
    protected $old_attributes = [];

    /**
     * @var Database
     */
    protected $db;
    /**
     * @var object of database query result(s)
     */
    protected $result;
    /**
     * @var string name of table; this property will fill in its' model
     */
    protected static $tableName;
    /**
     * @var array of query parts
     */
    protected static $query;
    /**
     * @var string of query command
     */
    protected static $command = '';
    /**
     * @var array of error list
     */
    protected static $errors = [];
    /**
     * @var bool return records as simple array
     */
    protected $returnAsArray = false;
    /**
     * @var QueryBuilder
     */
    protected static $instance;

    public function __construct(array $configs = [])
    {
        $this->isNewRecord = true;
        parent::__construct($configs);
    }

    /**
     * Initialize database QueryBuilder component.
     */
    public function init()
    {
        if (!isset(static::$instance))
            static::$instance = $this;
    }

    public function getDB()
    {
        if (is_null($this->db)) {
            if (isset($this->queryBuilder) && isset($this->queryBuilder->db))
                return $this->queryBuilder->db;

            $this->db = container()->DB->getConnection();
        }

        return $this->db;
    }

    /**
     * Returns record(s) as simple array.
     * This method should create by active database QueryBuilder.
     * @return iQueryBuilder
     */
    public function asArray(): iQueryBuilder
    {
        $this->returnAsArray = true;
        return $this;
    }

    /**
     * @return array of query statements
     */
    public function getQuery(): array
    {
        return static::$query;
    }

    /**
     * @return string of query string statement depends on current database syntax
     */
    public function getQueryString(): string
    {
        return static::$command;
    }

    /**
     * This method will return name of primary key filed.
     * This method should declare by database model class.
     * @return string
     */
    public static function primaryKey()
    {
        return 'id';
    }

    /**
     * Returns validation rules to validate via validate method.
     * @return array
     */
    public static function validation()
    {
        return [];
    }

    /**
     * Select records by condition or prepare to use other specific methods on record(s).
     * @param array $condition
     * @return iQueryBuilder
     */
    public static function find($condition = []): iQueryBuilder
    {
        $self = self::$instance;

        $self::$command = '';
        $self::$query = [
            'do' => 'SELECT',
            'fields' => '*'
        ];

        $self->where($condition);

        return $self;
    }

    /**
     * Determine specific column(s) for select query.
     * @param $columns
     * @return iQueryBuilder
     */
    public function select($columns): iQueryBuilder
    {
        if (empty($columns) || $columns == '*')
            $columns = '*';
        else
            $columns = (array)$columns;

        static::$query['fields'] = $columns;

        return $this;
    }

    /**
     * Add `ON` condition to the current query statement.
     * This method will process only `And` conditions at this time.
     * @example ['field' => null], ['filed', 'like', 'something'], [['field' => 'val'], ['filed', 'like', 'something']]
     *
     * @param $condition
     * @param string $operator
     * @param bool $value
     * @return iQueryBuilder
     */
    public function on($condition, $operator = '', $value = false): iQueryBuilder
    {
        $conditions = [];

        if (is_array($condition)) {
            foreach ($condition as $field => $value) {
                if (is_array($value)) {
                    foreach ($value as $sub_condition) {
                        $conditions[] = $this->conditionGenerator(
                            array_get($sub_condition, 0, ''),
                            array_get($sub_condition, 0, ''),
                            array_get($sub_condition, 2, false));
                    }
                } else {
                    $conditions[] = $this->conditionGenerator($field, $operator, $value);
                }
            }
        } elseif (is_string($condition) && !empty($condition)) {
            $conditions[] = $this->conditionGenerator($condition, $operator, $value);
        } else {
            return $this;
        }

        static::$query['on'] = array_merge(array_get(static::$query, 'on', []), $conditions);

        return $this;
    }

    /**
     * Add `WHERE` condition to the current query statement.
     * This method will process only `And` conditions at this time.
     * @example ['field' => null], ['filed', 'like', 'something'], [['field' => 'val'], ['filed', 'like', 'something']]
     *
     * @param $condition
     * @param string $operator
     * @param bool $value
     * @return iQueryBuilder
     */
    public function where($condition, $operator = '', $value = false): iQueryBuilder
    {
        $conditions = [];

        if (is_array($condition)) {
            foreach ($condition as $field => $sub_condition) {
                if (!is_array($sub_condition)) {
                    $is_not_processed = true;
                    break;
                }

                $conditions[] = $this->conditionGenerator(
                    array_get($sub_condition, 0, ''),
                    array_get($sub_condition, 1, ''),
                    array_get($sub_condition, 2, false)
                );
            }

            if (isset($is_not_processed)) {
                $conditions[] = $this->conditionGenerator(
                    array_get($condition, 0, ''),
                    array_get($condition, 1, ''),
                    array_get($condition, 2, false)
                );
            }
        } elseif (is_string($condition) && !empty($condition)) {
            $condition = $this->conditionGenerator($condition, $operator, $value);
            $conditions[] = $condition;
        } else {
            return $this;
        }

        static::$query['where'] = array_merge(array_get(static::$query, 'where', []), $conditions);

        return $this;
    }

    /**
     * Do insert/update current record.
     * @return bool
     */
    public function save(): bool
    {
        if ($this->validate() && static::beforeSave($this->isNewRecord())) {
            if ($this->isNewRecord()) {
                $success = $this->insert();
            } else {
                $success = $this->update();
            }

            if (!empty($this->db->error)) {
                $this->addError($this->db->error);
            }

            if ($success !== false) {
                if ($this->isNewRecord()) {
                    $this->isNewRecord = false;
                    $this->{static::primaryKey()} = $this->getInsertedID();
                }

                if ($this->afterSave() === false)
                    return false;

                return true;
            }
        }

        return false;
    }

    /**
     * Check does current record is new record for db or not.
     * This method will use for save() method to determine run insert() or update() method.
     * @return bool
     */
    public function isNewRecord(): bool
    {
        return $this->isNewRecord;
    }

    /**
     * Build insert query statement.
     * @param array $columns
     * @return bool
     */
    public function insert($columns = [])
    {
        if (array_key_exists('created_at', $this->attributes) && is_null($this->attributes['created_at']))
            $this->attributes['created_at'] = 'NOW()';

        $this->attributes = array_merge($this->attributes, $columns);

        static::$query = [
            'do' => 'INSERT',
            'fields' => array_keys($this->attributes),
            'values' => array_values($this->attributes)
        ];

        return true;
    }

    /**
     * Build update query statement.
     * @param array $columns
     * @return bool
     */
    public function update($columns = [])
    {
        if (array_key_exists('updated_at', $this->attributes) && is_null($this->attributes['updated_at']))
            $this->attributes['updated_at'] = 'NOW()';

        $this->attributes = array_merge($this->attributes, $columns);

        static::$query = [
            'do' => 'UPDATE',
            'fields' => array_keys($this->attributes),
            'values' => array_values($this->attributes)
        ];

        return true;
    }

    /**
     * Build delete query statement.
     * @param array $conditions
     * @return iQueryBuilder
     */
    public function delete($conditions = []): iQueryBuilder
    {
        $self = self::$instance;

        $self::$command = '';
        $self::$query = [
            'do' => 'DELETE'
        ];

        $self->where($conditions);

        return $self;
    }

    /**
     * Add `ORDER BY` query statement.
     * @param array $fields
     * @return iQueryBuilder
     */
    public function orderBy($fields = []): iQueryBuilder
    {
        if (!empty($fields)) {
            if (!is_array($fields)) {
                switch ($fields) {
                    case 'ASC':
                    case SORT_ASC:
                        static::$query['order_by'] = static::primaryKey() . ' ASC';
                        break;

                    case 'DESC':
                    case SORT_DESC:
                        static::$query['order_by'] = static::primaryKey() . ' DESC';
                        break;

                    default:
                        static::$query['order_by'] = $fields;
                        break;
                }
            } elseif (is_array($fields)) {
                foreach ($fields as $field => $sort_order) {
                    if (is_numeric($field)) {
                        $field = $sort_order;
                        $sort_order = 'ASC';
                    } elseif ($sort_order == SORT_ASC) {
                        $sort_order = 'ASC';
                    } elseif ($sort_order == SORT_DESC) {
                        $sort_order = 'DESC';
                    }

                    static::$query['order_by'][] = $field . ' ' . $sort_order;
                }

                static::$query['order_by'] = implode(', ', static::$query['order_by']);
            }
        }

        return $this;
    }

    /**
     * ADD `GROUP BY` query statement.
     * @param string $fields
     * @return iQueryBuilder
     */
    public function groupBy($fields = ''): iQueryBuilder
    {
        static::$query['group_by'] = $fields;

        return $this;
    }

    /**
     * Add field error.
     * @param $field
     * @param null $error
     */
    public function addError($field, $error = null)
    {
        if (!empty($field) && !empty($error)) {
            static::$errors[$field][] = $error;
        } elseif (!empty($field) && !isset($error)) {
            static::$errors['_syntax_'][] = $field;
        } else {
            static::$errors['_syntax_'][] = $error;
        }
    }

    /**
     * Add array of errors to current Model.
     * @param array $errors
     */
    public function addErrors($errors = [])
    {
        if (!empty($errors))
            array_merge(static::$errors, $errors);
    }

    /**
     * Set table attribute by magic method.
     * @param $name
     * @param $value
     * @throws ReadOnlyProperty
     */
    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        } elseif (property_exists($this, $name)) {
            try {
                $this->{$name} = $value;
            } catch (\Exception $e) {
                throw new ReadOnlyProperty($this::className(), $name);
            }
        } elseif (method_exists($this, 'get' . $name)) {
            throw new ReadOnlyProperty($this::className(), $name);
        } else {
            $this->attributes[$name] = $value;
        }
    }

    /**
     * get table attribute by magic method.
     * @param $name
     * @return mixed
     * @throws UnknownProperty
     */
    public function __get($name)
    {
        $getter = 'get' . ucfirst($name);
        if (method_exists($this, $getter))
            return $this->$getter($name);

        elseif (property_exists($this, $name))
            return $this->{$name};

        elseif (array_key_exists($name, $this->attributes))
            return $this->attributes[$name];

        throw new UnknownProperty($this::className(), $name);
    }

    /**
     * Returns one of selected query record(s).
     * This method should create by active database QueryBuilder.
     * @return null|mixed
     */
    abstract public function one();

    /**
     * Returns all of selected query record(s).
     * This method should create by active database QueryBuilder.
     * @return null|mixed
     */
    abstract public function all();

    /**
     * Returns parsed database query string to execute.
     * This method should create by active database QueryBuilder.
     * @return string
     */
    abstract public function createCommand();

    /**
     * Returns affected rows after insert, update and delete queries.
     * @return int
     */
    abstract public function affected_rows();

    /**
     * Returns inserted record id
     * @return int
     */
    abstract public function getInsertedID();

    /**
     * Parse condition statement and generate string depends on database syntax.
     * @param $field
     * @param string $operator
     * @param bool $value
     * @return string
     */
    abstract protected function conditionGenerator($field, $operator = '', $value = false);

    /**
     * Will fire before do save the record.
     * This event can validate after default model validation and then the record will be save if returns true.
     * @param bool $insert true if the record is new
     * @return bool
     */
    protected function beforeSave($insert = false)
    {
        return true;
    }

    /**
     * Will fires after save the record.
     * @param bool $insert true if the record is new
     * @return bool if returns false the save method will returns false
     */
    protected function afterSave($insert = false)
    {
        return true;
    }

}