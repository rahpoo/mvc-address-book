<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/30/2018
 * Time: 1:45 AM
 */

namespace core\db\mysql;


use core\contracts\iModel;
use core\contracts\iQueryBuilder;
use core\db\validators\iValidator;

/**
 * Class Model is base model class
 * @package core\db\mysql
 */
abstract class Model extends QueryBuilder implements iModel
{
    /**
     * @var QueryBuilder
     */
    protected $queryBuilder;
    /**
     * @var array cast types of model fields
     */
    protected $casts = [];
    /**
     * @var iValidator[] of model validators
     */
    protected $validators = [];
    /**
     * @var array of error list
     */
    protected static $errors = [];

    /**
     * @return string name of model table.
     */
    abstract static public function tableName();

    /**
     * @return array of table fields' labels.
     */
    abstract public function labels();

    /**
     * @return array of table fields.
     */
    abstract public function fields();

    public function __construct(array $configs = [])
    {
        if (is_null($this->queryBuilder))
            $configs = array_merge($configs, ['queryBuilder' => container()->get('QueryBuilder')]);

        $this->initAttributes();

        parent::__construct($configs);
        parent::$command = '';
        parent::$tableName = static::tableName();

        return $this;
    }

    /**
     * Prepare model to select record(s).
     * @param array $condition
     * @return iQueryBuilder
     */
    public static function find($condition = []): iQueryBuilder
    {
        parent::$tableName = static::tableName();
        $parent = parent::find($condition);

        return (new static(['queryBuilder' => $parent, 'db' => $parent->db, 'isNewRecord' => false]));
    }

    /**
     * Returns one record using primaryKey field.
     * @param $id
     * @return array|Model|mixed|null
     */
    public static function findOneById($id)
    {
        parent::$tableName = static::tableName();
        $self = static::find([static::primaryKey(), '=', intval($id)]);
        $model = (new static(['queryBuilder' => $self, 'db' => $self->db, 'isNewRecord' => false]));

        return $model->one();
    }

    public function delete($conditions = [])
    {
        parent::$tableName = static::tableName();

        return parent::delete($conditions);
    }

    /**
     * Returns first record.
     * @return array|Model|mixed|null
     */
    public function one()
    {
        if ($record = $this->queryBuilder->one()) {
            return $this->load($record);
        }

        return null;
    }

    /**
     * Returns all records.
     * @return array|mixed|null
     */
    public function all()
    {
        $models = [];

        if ($records = $this->queryBuilder->all()) {
            foreach ($records as $record) {
                $model = new static(['isNewRecord' => false]);
                $model = $model->load((array)$record);
                if ($this->returnAsArray === true)
                    $model = $model->attributes;

                $models[] = $model;
            }
        }

        return $models;
    }

    /**
     * Load specific record as model object.
     * @param array $data
     * @return static
     */
    public function load(array $data)
    {
        $this->initAttributes();

        foreach ($data as $field => $value) {
            $this->{$field} = $value;
            $this->old_attributes[$field] = $value;
        }

        $tableName = self::$tableName;
        $this->afterFind();
        self::$tableName = $tableName;

        return $this;
    }

    /**
     * Returns model attributes after do type casting.
     * @return mixed
     */
    public function attributes()
    {
        $this->doTypeCasts();

        return $this->attributes;
    }

    /**
     * Returns labels of current model's and linked model's fields.
     * @param array $fields
     * @return array
     */
    public function getFieldLabels(array $fields = [])
    {
        $labels = [];
        $_labels = $this->labels();

        if (empty($fields))
            $fields = $this->fields();

        foreach ($fields as $field) {
            if (is_array($field)) {
                $_field = array_get($field, 'field');

                if ($label = array_get($field, 'label')) {
                    $labels[$_field] = $label;
                } else {
                    $labels[$_field] = $this->smartLabelGenerator($_labels, $_field);
                }
            } else {
                $labels[$field] = $this->smartLabelGenerator($_labels, $field);
            }
        }

        return $labels;
    }

    /**
     * Returns label of field.
     * @param $field
     * @return mixed|null
     */
    public function getFieldLabel($field)
    {
        if (!array_key_exists($field, $this->attributes) && !array_key_exists($field, $this->labels()))
            return null;

        $labels = $this->labels();

        return $this->smartLabelGenerator($labels, $field);
    }

    /**
     * Returns field's label of moder or sub model's fields.
     * @param $labels
     * @param $field
     * @return mixed|null
     */
    protected function smartLabelGenerator($labels, $field)
    {
        if (array_key_exists($field, $labels))
            return $labels[$field];
        else {
            $model = $this;

            if (strstr($field, '.')) {
                $_sub_field = explode('.', $field);

                foreach ($_sub_field as $sub_field) {
                    if (!is_object($model->{$sub_field})) {
                        $field = $sub_field;
                        break;
                    }

                    $model = $model->{$sub_field};
                }
            }

            return array_get($labels, $field, $model->generateFieldLabel($field));
        }
    }

    /**
     * @param $name
     * @return mixed|string
     */
    public function generateFieldLabel($name)
    {
        return camel_to_word($name, true);
    }

    /**
     * Validate model attributes.
     * @param null $fieldNames
     * @return bool
     */
    public function validate($fieldNames = null): bool
    {
        $this->generateValidators();

        foreach ($this->validators as $validator) {
            $validator->validate();
        }

        if (!empty(static::$errors))
            return false;

        return true;
    }

    /**
     * Generate model validator objects.
     */
    protected function generateValidators()
    {
        $validatorNamespace = '\\core\\db\\validators\\';
        $validationRules = $this->validation();

        foreach ($validationRules as $vRule) {
            $fields = array_get($vRule, 0, []);
            $validator = array_get($vRule, 1);
            $params = array_get($vRule, 2, []);
            $validatorClass = $validatorNamespace . camel_to_word($validator, true);

            if (!class_exists($validatorClass))
                continue;

            $this->validators[$validator] = new $validatorClass($this, $fields, $params);
        }
    }

    /**
     * Returns array of model errors.
     * @return array
     */
    public function getErrors()
    {
        return static::$errors;
    }

    /**
     * Do type casting for model attributes.
     */
    public function doTypeCasts()
    {
        foreach ($this->casts as $field => $type) {
            typecast($this->attributes[$field], $type);
        }
    }

    protected function initAttributes()
    {
        if (is_null(static::$query['fields']) || empty(static::$query['fields']) || static::$query['fields'] == '*')
            $fields = $this->fields();
        else
            $fields = static::$query['fields'];

        foreach ($fields as $field) {
            $this->{$field} = null;
        }
    }

    /**
     * Returns One to One relational model.
     * @param iModel $model
     * @param array $link
     * @return mixed
     */
    protected function hasOne(iModel $model, $link = [])
    {
        $tableName = static::$tableName;
        $query = static::$query;
        $subModel = $model::find()->where($link[0], '=', $this->{$link[1]})->orderBy(SORT_ASC)->one();
        static::$query = $query;
        static::$tableName = $tableName;

        return $subModel;
    }

    /**
     * Returns One to Many relational model.
     * @param iModel $model
     * @param array $link
     * @return mixed
     */
    protected function hasMany(iModel $model, $link = [])
    {
        return $model::find()->where($link[0], '=', $this->{$link[1]})->orderBy(SORT_ASC)->all();
    }

    /**
     * This event will do something after find record(s).
     */
    protected function afterFind()
    {

    }

    /**
     * This event will do something or validate before save anything.
     * @param bool $insert
     * @return bool
     */
    protected function beforeSave($insert = false)
    {
        return true;
    }

    /**
     * This event will do something or validate after do save.
     * @param bool $insert
     * @return bool
     */
    protected function afterSave($insert = false)
    {
        return true;
    }
}