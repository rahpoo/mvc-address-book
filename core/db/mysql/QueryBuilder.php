<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/30/2018
 * Time: 1:05 !M
 */

namespace core\db\mysql;

use core\contracts\iModel;

/**
 * Class QueryBuilder
 * @package core\db\mysql
 *
 * @property \mysqli $db
 */
class QueryBuilder extends \core\db\QueryBuilder
{
    /**
     * @var \mysqli_result|\mysqli_stmt resource of mysql query result(s)
     */
    protected $result;

    /**
     * Set false auto commit to build custom transactions.
     * @return bool
     */
    public function beginTransaction()
    {
        if (is_null($this->db))
            $this->db = $this->getDB();

        $transaction = $this->db->begin_transaction();

        if (!$transaction) {
            $this->addError($this->db->error);
            return false;
        }

        return true;
    }

    /**
     * Do commit custom transaction.
     */
    public function commit()
    {
        $this->db->commit();
    }

    /**
     * Do rollback custom transaction.
     */
    public function rollback()
    {
        $this->db->rollback();
    }

    /**
     * Returns one iModel record if exists otherwise null.
     * @return iModel|null
     */
    public function one()
    {
        $this->executeQuery();

        if ($this->result instanceof \mysqli_result)
            return mysqli_fetch_assoc($this->result);

        return null;
    }

    /**
     * Returns all found records as array or otherwise array.
     * Returns null if there isn't any mysqli_result exists.
     * @return iModel[]|null
     */
    public function all()
    {
        $this->executeQuery();

        if ($this->result instanceof \mysqli_result) {
            return mysqli_fetch_all($this->result, MYSQLI_ASSOC);
        }

        return null;
    }

    /**
     * Delete record(s).
     * @param array $conditions optional extra conditions for deletion
     * @return bool|\core\db\QueryBuilder
     */
    public function delete($conditions = [])
    {
        parent::delete($conditions);
        return $this->executeQuery();
    }

    /**
     * @param array $columns
     * @return bool
     */
    public function insert($columns = [])
    {
        parent::insert($columns);
        return $this->executeQuery();
    }

    /**
     * @param array $columns
     * @return bool
     */
    public function update($columns = [])
    {
        parent::update($columns);
        return $this->executeQuery();
    }

    /**
     * Returns affected rows after insert, update and delete queries.
     * @return int
     */
    public function affected_rows()
    {
        return $this->result->affected_rows;
    }

    /**
     * Returns primary key of inserted record.
     * @return int
     */
    public function getInsertedID()
    {
        return $this->result->insert_id;
    }

    /**
     * Run query string.
     * @return bool
     */
    public function executeQuery()
    {
        $db = $this->getDB();

        $this->createCommand();

        $stmt = $db->prepare(static::getQueryString());

        if ($stmt instanceof \mysqli_stmt) {
            $this->bindParams($stmt)->execute();
            $this->result = $stmt->get_result();

            if ($this->result === false)
                $this->result = $stmt;
        }

        if (!empty($db->error)) {
            $this->addError($db->error);
            return false;
        }

        return $stmt;
    }

    /**
     * Create and returns query command to execute.
     * @return string
     */
    public function createCommand()
    {
        static::$command = array_get(static::$query, 'do', '');

        switch (static::$command) {
            case 'SELECT':
                $fields = array_get(static::$query, 'fields');

                if ($fields != '*')
                    $fields = '`' . implode('`, `', $fields) . '`';

                static::$command .= ' ' . $fields;
                static::$command .= ' FROM `' . static::$tableName . '`';
                break;

            case 'INSERT':
                static::$command .= ' INTO ' . static::$tableName . ' ';
                $this->appendInsertParams();
                break;

            case 'UPDATE':
                static::$command .= ' `' . static::$tableName . '` SET ';
                $this->appendUpdateParams();

                if (!isset(static::$query['where']) || empty(static::$query['where'])) {
                    $primaryKey = static::primaryKey();
                    $this->where($primaryKey, $this->{$primaryKey});
                }
                break;

            case 'DELETE':
                static::$command .= ' FROM `' . static::$tableName . '`';

                if (!isset(static::$query['where']) || empty(static::$query['where'])) {
                    $primaryKey = static::primaryKey();
                    $this->where($primaryKey, $this->{$primaryKey});
                }
                break;
        }

        if (!empty(static::$command) && !isset($insert)) {
            if ($on_condition = array_get(static::$query, 'on')) {
                static::$command .= ' ON ' . implode(' AND ', $on_condition);
            }

            if ($where = array_get(static::$query, 'where')) {
                static::$command .= ' WHERE ' . implode(' AND ', $where);
            }

            if ($sort_order = array_get(static::$query, 'order_by')) {
                static::$command .= ' ORDER BY ' . $sort_order;
            }

            if ($group_by = array_get(static::$query, 'group_by')) {
                static::$command .= ' GROUP BY ' . $group_by;
            }
        }

        return static::$command;
    }

    /**
     * Append insert parameters to current insert query statement.
     */
    protected function appendInsertParams()
    {
        $values_params = '';
        $fields = array_get(static::$query, 'fields', []);
        $values = array_get(static::$query, 'values', []);

        foreach ($fields as $index => $field) {
            $values_params .= ", " . $this->addParam(array_get($values, $index));
        }

        static::$command .= '(`' . implode('`, `', array_get(static::$query, 'fields', [])) . '`)';
        static::$command .= ' VALUES (' . ltrim($values_params, ', ') . ')';
    }

    /**
     * Append update parameters to current update query statement.
     */
    protected function appendUpdateParams()
    {
        $statement = '';
        $fields = array_get(static::$query, 'fields', []);
        $values = array_get(static::$query, 'values', []);

        foreach ($fields as $index => $field) {
            if ($field == static::primaryKey())
                continue;

            $statement .= ", `$field` = " . $this->addParam(array_get($values, $index));
        }

        static::$command .= ltrim($statement, ', ');
    }

    /**
     * Add a value to bind params.
     * @param $value
     * @param bool $insert_before
     * @return string
     */
    protected function addParam($value, $insert_before = false)
    {
        $prev_params = [];

        if ($insert_before === true) {
            $prev_params = static::$query['params'];
            static::$query['params'] = [];
        }

        switch (strtolower($value)) {
            case 'null':
                $param = "null";
                break;
            case 'now()':
                $param = "NOW()";
                break;

            default:
                static::$query['params'][] = $value;
                $param = '?';
                break;
        }

        if (!empty($prev_params)) {
            static::$query['params'] = array_merge(static::$query['params'], $prev_params);
        }

        return $param;
    }

    /**
     * Parse condition statement and generate string.
     * @param $field
     * @param string $operator
     * @param null|mixed $value
     * @return string
     */
    protected function conditionGenerator($field, $operator = '', $value = null)
    {
        if (is_null($field) || empty($field))
            return '';

        if (is_null($operator) || (!is_null($operator) && is_null($value)))
            return $condition = "`$field` IS NULL";

        if (is_array($operator)) {
            $values = ltrim(str_repeat(', ?', count($operator)), ', ');
            static::$query['params'] = array_merge(array_get(static::$query, 'params', []), $operator);

            return "`$field` IN ($values)";
        }

        switch (strtolower($operator)) {
            case null;
                return "`$field` IS NULL";
                break;

            case 'not null':
                return "`$field` IS NOT NULL";
                break;

            case 'now()':
                return "`$field` = NOW()";
                break;

            case '=':
                static::$query['params'][] = $value;
                return "`$field` = ?";
                break;

            case '!=':
                static::$query['params'][] = $value;
                return "`$field` != ?";
                break;

            case 'like':
                static::$query['params'][] = $value;
                return "`$field` LIKE ?";
                break;

            case 'in':
                if (is_array($value)) {
                    $values = ltrim(str_repeat(', ?', count($value)), ', ');
                    static::$query['params'] = array_merge(array_get(static::$query, 'params', []), $value);
                } else {
                    $values = "?";
                    static::$query['params'][] = $value;
                }

                return "`$field` IN ($values)";
                break;

            case 'not in':
                if (is_array($value)) {
                    $values = ltrim(str_repeat(', ?', count($value)), ', ');
                    static::$query['params'] = array_merge(array_get(static::$query, 'params', []), $value);
                } else {
                    $values = "?";
                    static::$query['params'][] = $value;
                }

                return "`$field` NOT IN ($values)";
                break;
        }

        if (!empty($operator) && $value === false) {
            static::$query['params'][] = $operator;
            return "`$field` = ?";
        }
    }

    /**
     * Bind params via mysqli_stmt object.
     * @param \mysqli_stmt $stmt
     * @param array $params
     * @return \mysqli_stmt
     */
    protected function bindParams(\mysqli_stmt $stmt, array $params = [])
    {
        $types = '';
        $params = empty($params) ? array_get(static::$query, 'params', []) : $params;

        if (!empty($params)) {
            foreach ($params as $field => $param) {
                $types .= $this->getType($param);

                $_{$field} = $param;
                $params[$field] = &$_{$field};
            }

            call_user_func_array([&$stmt, 'bind_param'], array_merge([$types], $params));
        }

        return $stmt;
    }

    /**
     * Returns type of mysqli bind_param.
     * @param $param
     * @return string
     */
    public function getType($param)
    {
        switch (gettype($param)) {
            default:
            case 'string':
                return 's';
                break;

            case 'integer':
            case 'int':
            case 'boolean':
            case 'bool':
                return 'i';
                break;

            case 'double':
            case 'float':
                return 'd';
                break;

            case 'blob':
                return 'b';
                break;

        }
    }
}