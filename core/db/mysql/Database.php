<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/30/2018
 * Time: 12:59 AM
 */

namespace core\db\mysql;


class Database extends \core\db\Database
{
    public function init()
    {
        $this->connect();

        container()->assign('QueryBuilder', QueryBuilder::className(), ['db' => $this->connection]);
    }

    public function connect()
    {
        $this->connection = mysqli_connect($this->hostname, $this->username, $this->password, $this->database, $this->port);

        /* check connection */
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }

        mysqli_set_charset($this->connection, $this->charset);
    }

    public function disconnect()
    {
        mysqli_close($this->connection);
    }
}