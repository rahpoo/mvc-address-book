<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/30/2018
 * Time: 12:35 AM
 */

namespace core\db;


use core\base\Component;
use core\contracts\iDBConnection;

abstract class Database extends Component implements iDBConnection
{
    public $hostname;
    public $port;
    public $database;
    public $username;
    public $password;
    public $charset;

    protected $connection;

    public function init()
    {
        $this->connect();
    }

    abstract public function connect();

    abstract public function disconnect();

    public function getConnection()
    {
        return $this->connection;
    }
}