<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 4:26 PM
 */

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', realpath('./') . DS);
define('FILES', ROOT . DS . 'uploads' . DS);

$loader = require ROOT . 'vendor' . DS . 'autoload.php';
require_once 'helpers' . DS . 'Helpers.php';

$Container = \core\Container::getInstance();

/**
 * @var \core\base\Config
 */
$Config = $Container->assign('Config', '\core\base\Config');

if ($dbConfig = $Config->get('db')) {
    /**
     * @var \core\contracts\iDBConnection
     */
    $DB = $Container->assign('DB', $dbConfig);
}

$Container->set('Template', '\core\base\Template');

$Container->assign('Router', '\core\base\Router', $Config->get('router', []));
