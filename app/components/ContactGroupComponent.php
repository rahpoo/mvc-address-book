<?php
/**
 * Created by PhpStorm.
 * User: meysam.ghanbari
 * Date: 2018-12-17
 * Time: 22:46
 */

namespace app\components;


use app\models\ContactGroup;
use core\base\Component;

/**
 * Component ContactGroup
 * @package app\components
 */
class ContactGroupComponent extends Component
{
    /**
     * Returns all nested groups as navbar.
     * @return string
     */
    public function getContactGroupsNavbar()
    {
        $output = '';
        $groups = ContactGroup::find()->where('contact_group_parent_id', null)->all();
        $subGroups = ContactGroup::find()->where('contact_group_parent_id', 'not null')->all();

        foreach ($groups as $group) {
            $output .= '<li><a href="index?by_group=' . $group->contact_group_id . '">' . $group->contact_group_name . '</a>';
            $output .= $this->getNestedSubGroupsNavbar($subGroups, $group->contact_group_id);
            $output .= '</li>';
        }

        return $output;
    }

    /**
     * Internal method to get all nested groups for navbar.
     * @param $subGroups
     * @param null $parent_id
     * @return string
     */
    protected function getNestedSubGroupsNavbar(&$subGroups, $parent_id = null)
    {
        $output = '';

        foreach ($subGroups as $index => $subGroup) {
            if ($subGroup->contact_group_parent_id != $parent_id)
                continue;

            unset($subGroups[$index]);

            $output .= '<li><a href="index?by_group=' . $subGroup->contact_group_id . '">' . $subGroup->contact_group_name . '</a>';
            $output .= $this->getNestedSubGroupsNavbar($subGroups, $subGroup->contact_group_id);
            $output .= '</li>';
        }

        if (!empty($output))
            $output = '<ul>' . $output . '</ul>';

        return $output;
    }
}