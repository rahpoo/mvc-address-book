<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 12:10 AM
 */

return [
    'db' => [
        'class' => '\\core\\db\\mysql\\Database',

        'hostname' => 'localhost',
        'port' => '3306',
        'database' => 'address_book',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8'
    ],

    'router' => [
        'controllerPath' => 'app/controllers',
        'controllerNamespace' => '\\app\\controllers\\',
    ]
];