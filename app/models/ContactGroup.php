<?php
/**
 * Created by PhpStorm.
 * User: meysam.ghanbari
 * Date: 2018-12-13
 * Time: 23:33
 */

namespace app\models;


use core\db\mysql\Model;

/**
 * Model ContactGroup
 * @package app\models
 *
 * @property int $contact_group_id;
 * @property int $contact_group_parent_id;
 * @property string $contact_group_name;
 *
 * @property ContactGroup $parent
 * @property ContactGroup $child
 * @property ContactGroupAssignment $assignments
 * @property Contact[] $contacts
 */
class ContactGroup extends Model
{
    protected $casts = [
        'contact_group_id' => 'int',
        'contact_group_parent_id' => 'int'
    ];

    public static function tableName()
    {
        return 'contact_group';
    }

    public static function primaryKey()
    {
        return 'contact_group_id';
    }

    public static function validation()
    {
        return [
            [['contact_group_name'], 'required'],
            [['contact_group_name'], 'length', [50]],
        ];
    }

    public function fields()
    {
        return [
            'contact_group_id',
            'contact_group_parent_id',
            'contact_group_name'
        ];
    }

    public function labels()
    {
        return [
            'contact_group_id' => 'Group ID',
            'contact_group_parent_id' => 'Parent Group',
            'contact_group_name' => 'Group Name'
        ];
    }

    protected function beforeSave($insert = false)
    {
        if (!parent::beforeSave($insert))
            return false;

        if ($this->contact_group_parent_id <= 0)
            return true;

        $parentGroup = $this->parent;

        if (!$parentGroup) {
            $this->addError('contact_group_parent_id', 'Was not found!');
            return false;
        }

        return $this->beginTransaction();
    }

    protected function afterSave($insert = false)
    {
        if (!parent::afterSave($insert))
            return false;

        if ($this->contact_group_parent_id <= 0)
            return true;

        $parentContacts = $this->parent->contacts;

        if (!empty($parentContacts)) {
            foreach ($parentContacts as $contact) {
                $assignment = new ContactGroupAssignment();
                $assignment->groupid = $this->contact_group_id;
                $assignment->contactid = $contact->id;

                if (!$assignment->save()) {
                    $this->rollback();
                    $this->addErrors($assignment->getErrors());
                    return false;
                }
            }
        }

        $this->commit();
        return true;
    }

    /**
     * Returns parent group.
     * @return ContactGroup
     */
    public function getParent()
    {
        return $this->hasOne(new ContactGroup(), ['contact_group_id', 'contact_group_parent_id']);
    }

    /**
     * Returns child group.
     * @return ContactGroup
     */
    public function getChild()
    {
        return $this->hasOne(new ContactGroup(), ['contact_group_parent_id', 'contact_group_id']);
    }

    /**
     * Returns assignments of current contact.
     * @return array|ContactGroupAssignment[] empty array if not exists anything
     */
    public function getAssignments()
    {
        return $this->hasMany(new ContactGroupAssignment(), ['groupid', 'contact_group_id']);
    }

    public function getContacts()
    {
        $contacts = ContactGroupAssignment::find(['groupid', $this->contact_group_id])->select('contactid')->asArray()->all();
        $contacts = array_flat($contacts);

        return Contact::find(['id', $contacts])->all();
    }

    /**
     * Returns all nested children of a group.
     * @param null $prev_group_id
     * @return array
     */
    public function getAllChildGroups($prev_group_id = null)
    {
        if (is_null($prev_group_id))
            $prev_group_id = $this->contact_group_id;

        $subGroups = self::find()
            ->where('contact_group_parent_id', 'not null')
            ->where('contact_group_id', '!=', $prev_group_id)
            ->all();

        return $this->getNestedSubGroups($subGroups, $prev_group_id);
    }

    /**
     * Internal method to get all nested child groups of specific group.
     * @param $subGroups
     * @param null $parent_id
     * @return array
     */
    protected function getNestedSubGroups(&$subGroups, $parent_id = null)
    {
        $output = [];

        foreach ($subGroups as $index => $subGroup) {
            if ($subGroup->contact_group_parent_id != $parent_id)
                continue;

            unset($subGroups[$index]);

            $output[] = $subGroup;

            $output = array_merge($output, $this->getNestedSubGroups($subGroups, $subGroup->contact_group_id));
        }

        return $output;
    }

    /**
     * Returns multi-level groups by steps.
     *
     * this method will use for select boxes and DataGrid view.
     *
     * @param null $exclude_group_id
     * @param bool $name_only
     * @return array
     */
    public function getStepsContactGroups($exclude_group_id = null, $name_only = true)
    {
        $output = [];
        $groups = ContactGroup::find()->where('contact_group_parent_id', null);

        if (!is_null($exclude_group_id))
            $groups->where('contact_group_id', '!=', $exclude_group_id);

        $groups = $groups->all();

        $subGroups = self::find()->where('contact_group_parent_id', 'not null');

        if (!is_null($exclude_group_id))
            $subGroups->where('contact_group_id', '!=', $exclude_group_id);

        $subGroups = $subGroups->all();

        foreach ($groups as $group) {
            $level = 0;
            $output[$group->contact_group_id] = $name_only === true ? $group->contact_group_name : $group;

            $output += $this->getStepsContactSubGroups($subGroups, $group->contact_group_id, $level, $name_only);
        }

        return $output;
    }

    /**
     * Internal method to get sub groups for step multi-level groups.
     * @param $subGroups
     * @param $parent_id
     * @param $level
     * @param bool $name_only
     * @return array
     */
    protected function getStepsContactSubGroups(&$subGroups, $parent_id, &$level, $name_only = true)
    {
        $level++;
        $output = [];

        foreach ($subGroups as $index => $subGroup) {
            if ($subGroup->contact_group_parent_id != $parent_id)
                continue;

            unset($subGroups[$index]);

            if ($name_only === true)
                $output[$subGroup->contact_group_id] = str_repeat('-', $level) . ' ' . $subGroup->contact_group_name;
            else {
                $subGroup->contact_group_name = str_repeat('-', $level) . ' ' . $subGroup->contact_group_name;
                $output[$subGroup->contact_group_id] = $subGroup;
            }


            $output += $this->getStepsContactSubGroups($subGroups, $subGroup->contact_group_id, $level, $name_only);
            $level--;
        }

        return $output;
    }
}