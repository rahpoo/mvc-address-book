<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 11/13/2018
 * Time: 1:08 AM
 */

namespace app\models;


use core\db\mysql\Model;

/**
 * Model City
 * @package app\models
 *
 * @property int $city_id
 * @property string $city_name
 */
class City extends Model
{
    public static function tableName()
    {
        return 'city';
    }

    public static function primaryKey()
    {
        return 'city_id';
    }

    public static function validation()
    {
        return [
            [['city_name'], 'required'],
            [['city_name'], 'length', [2]],
        ];
    }

    public function fields()
    {
        return [
            'city_id',
            'city_name'
        ];
    }

    public function labels()
    {
        return [
            'city_id' => 'City ID',
            'city_name' => 'City Name'
        ];
    }
}