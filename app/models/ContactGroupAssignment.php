<?php
/**
 * Created by PhpStorm.
 * User: meysam.ghanbari
 * Date: 2018-12-13
 * Time: 23:34
 */

namespace app\models;


use core\db\mysql\Model;

/**
 * Model ContactGroupAssignment
 * @package app\models
 *
 * @property int $cg_assignment_id
 * @property int $contactid
 * @property int $groupid
 * @property boolean $is_owner
 *
 * @property Contact $contact
 * @property ContactGroup $group
 */
class ContactGroupAssignment extends Model
{
    protected $casts = [
        'contactid' => 'int',
        'groupid' => 'int',
        'is_owner' => 'int'
    ];

    public static function tableName()
    {
        return 'contact_group_assignment';
    }

    public static function primaryKey()
    {
        return 'cg_assignment_id';
    }

    public static function validation()
    {
        return [
            [['contactid', 'groupid'], 'required']
        ];
    }

    public function fields()
    {
        return [
            'cg_assignment_id',
            'contactid',
            'groupid',
            'is_owner'
        ];
    }

    public function labels()
    {
        return [
            'cg_assignment_id' => 'Group Assignment ID',
            'contactid' => 'Contact',
            'groupid' => 'Group',
            'is_owner' => 'Is Owner'
        ];
    }

    public function getContact()
    {
        return $this->hasOne(new Contact(), ['id', 'contactid']);
    }

    public function getGroup()
    {
        return $this->hasOne(new ContactGroup(), ['contact_group_id', 'groupid']);
    }
}