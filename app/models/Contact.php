<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/30/2018
 * Time: 3:12 PM
 */

namespace app\models;


use core\db\mysql\Model;

/**
 * Model Contact
 * @package app\models
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property int $cityid
 * @property string $street
 * @property int $zip_code
 * @property string $created_at
 * @property string $updated_at
 * @property int $group_id
 * @property int $groupID is a method to get owner group's id
 *
 * @property ContactGroupAssignment[] $assignments
 * @property ContactGroup $group
 * @property City $city
 */
class Contact extends Model
{
    public $group_id;

    protected $casts = [
        'id' => 'int',
        'cityid' => 'int',
        'zip_code' => 'int'
    ];

    public static function tableName()
    {
        return 'contact';
    }

    public static function validation()
    {
        return [
            [['firstname', 'lastname', 'cityid', 'street'], 'required'],
            [['zip_code'], 'integer'],
            [['zip_code'], 'length', [5, 10]],
        ];
    }

    public function fields()
    {
        return [
            'id',
            'firstname',
            'lastname',
            'cityid',
            'street',
            'zip_code',
            'created_at',
            'updated_at',
            'group_id'
        ];
    }

    public function labels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'cityid' => 'City',
            'group_id' => 'Group'
        ];
    }

    /**
     * @inheritdoc
     */
    protected function afterFind()
    {
        $this->old_attributes['group_id'] = $this->groupID;
    }

    /**
     * @inheritdoc
     */
    protected function beforeSave($insert = false)
    {
        if (!parent::beforeSave($insert))
            return false;

        $contact = Contact::find([
            ['firstname', $this->firstname],
            ['lastname', $this->lastname]
        ])->one();

        if ($contact) {
            $this->isNewRecord = false;
            $this->id = $contact->id;
        }

        return $this->beginTransaction();
    }

    /**
     * @inheritdoc
     */
    protected function afterSave($insert = false)
    {
        if (!parent::afterSave($insert))
            return false;

        // If user set a group for contact
        if (!is_null($this->group_id)) {

            // Delete all exists group assignments before insert/update contact
            (new ContactGroupAssignment())->delete(['contactid', $this->id]);

            $contactGroup = ContactGroup::findOneById($this->group_id);

            if (!$contactGroup) {
                $this->addError('group_id', 'Your selected group was not found!');
                return false;
            }

            $contactGroups = $contactGroup->getAllChildGroups();

            // Assign current contact to selected group by user
            $assignment = new ContactGroupAssignment();
            $assignment->groupid = $this->group_id;
            $assignment->contactid = $this->id;
            $assignment->is_owner = 1;

            if ($assignment->save()) {
                foreach ($contactGroups as $contactGroup) {
                    $assignment = new ContactGroupAssignment();
                    $assignment->groupid = $contactGroup->contact_group_id;
                    $assignment->contactid = $this->id;

                    if (!$assignment->save()) {
                        $this->rollback();
                        $this->addErrors($assignment->getErrors());
                        return false;
                    }
                }
            } else {
                $this->rollback();
                $this->addErrors($assignment->getErrors());
                return false;
            }
        }

        $this->commit();
        return true;
    }

    /**
     * Returns assignments of current contact.
     * @return array|ContactGroupAssignment[] empty array if not exists anything
     */
    public function getAssignments()
    {
        return $this->hasMany(new ContactGroupAssignment(), ['contactid', 'id']);
    }

    /**
     * Returns group assignment by group id
     * @param $id
     * @return null|ContactGroupAssignment
     */
    public function getAssignmentByGroupID($id)
    {
        return ContactGroupAssignment::find([
            ['contactid', $this->id],
            ['groupid', $id]
        ])->one();
    }

    /**
     * Returns owner group;
     * @return null|ContactGroup
     */
    public function getGroup()
    {
        $owner = ContactGroupAssignment::find([
            ['contactid', $this->id],
            ['is_owner', 1]
        ])->one();

        if ($owner)
            return $owner->group;

        return null;
    }

    /**
     * Returns id of owner group of current contact.
     * @return null|int
     */
    public function getGroupID()
    {
        $groupAssignment = ContactGroupAssignment::find([
            ['contactid', $this->id],
            ['is_owner', 1]
        ])->one();

        if ($groupAssignment)
            return $groupAssignment->groupid;

        return null;
    }

    /**
     * Returns city model if any city assigned to current contact.
     * @return null|City
     */
    public function getCity()
    {
        return $this->hasOne(new City(), ['city_id', 'cityid']);
    }
}