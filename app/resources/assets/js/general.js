$(document).ready(function () {
    $('.action-delete').on('click', function (e) {
        e.preventDefault();

        var delUrl = $(this).attr("href");

        if (confirm('Do you really want to remove this item?')) {
            $.ajax({
                url: delUrl,
                dataType: "json"
            }).done(function (data) {
                if (data) {
                    if (data.status === true)
                        var msg = data.result || 'The item has ben deleted successfully.';
                    else
                        var msg = data.errorMsg || 'Deleting current item has been failed!';

                    alert(msg);
                    window.location = 'index';
                }
            });
        }
    });
});