<?php
$form = \core\widgets\FormWidget::begin($model, urlTo('contact-groups/create'));
?>
<div class="row">
    <div class="col-sm-9">
        <h4>Add a new contact group</h4>
    </div>
    <div class="col-sm-3">
        <a href="index" class="btn btn-default pull-right">Go To List</a>
    </div>
</div>

<div class="row">
    <?php $this->view('_form', compact('form', 'model')) ?>
</div>