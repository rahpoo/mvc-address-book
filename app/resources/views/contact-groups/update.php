<?php
/**
 * @var \app\models\Contact $model
 */
$form = \core\widgets\FormWidget::begin($model, urlTo('contact-groups/edit?id=' . $id));
?>
<div class="row">
    <div class="col-sm-9">
        <h4>Edit address #<?= $model->contact_group_id ?></h4>
    </div>
    <div class="col-sm-3">
        <a href="index" class="btn btn-default pull-right">Go To List</a>
        <a href="delete?id=<?= $model->contact_group_id; ?>" class="btn btn-danger action-btn action-delete">Delete</a>
    </div>
</div>

<div class="row">
    <?php $this->view('_form', compact('form', 'model')) ?>
</div>