<?php
/**
 * @var \core\widgets\FormWidget $form
 * @var \app\models\Contact $model
 */

$button_title = 'Add Group';
$exclude_group_id = null;

if (!$model->isNewRecord()) {
    $button_title = 'Update Group';
    $exclude_group_id = $model->contact_group_id;
}

$groups = [null => '-- Root --'];
$groups += (new \app\models\ContactGroup())->getStepsContactGroups($exclude_group_id);

$form->field('selectbox', 'contact_group_parent_id')->items($groups);
$form->field('text', 'contact_group_name');

$form->button('submit', $button_title);

$form->render();