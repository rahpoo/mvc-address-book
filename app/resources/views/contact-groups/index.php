<?php
/**
 * @var \app\models\ContactGroup $contactGroup
 * @var \app\models\ContactGroup[] $contactGroups
 */
?>
<div class="row">
    <div class="col-sm-6">
        <h4>Contact Groups</h4>
    </div>
    <div class="col-sm-6 text-right">
        <a href="<?= urlTo('contact-groups/create') ?>" class="btn btn-success action-btn">New Group</a>
        <a href="<?= urlTo('default/index') ?>" class="btn btn-default action-btn">Manage Contacts</a>
    </div>
</div>

<div class="row">
    <div class="col-sm-10 center-block">
        <?php
        \core\widgets\GridViewWidget::begin($contactGroup, $contactGroups,
            [
                'contact_group_id',

                /*[
                    'field' => 'parent.contact_group_name',
                    'label' => 'Parent Group'
                ],*/

                'contact_group_name'
            ],
            [
                'table' => [
                    'class' => 'table table-striped table-hover'
                ],
                'header' => [
                    'style' => 'background: blue; color: #fff'
                ],
            ])->render();
        ?>
    </div>
</div>

