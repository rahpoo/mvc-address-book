<?php
/**
* @var \app\models\Contact $model
*/
?>
<div class="row">
    <div class="col-sm-9">
        <h4>View address #<?= $model->contact_group_id ?></h4>
    </div>
    <div class="col-sm-3">
        <a href="index" class="btn btn-default pull-right">Go To List</a>
        <a href="edit?id=<?= $model->contact_group_id; ?>" class="btn btn-success action-btn">Edit</a>
        <a href="delete?id=<?= $model->contact_group_id; ?>" class="btn btn-danger action-btn action-delete">Delete</a>
    </div>
</div>

<div class="row">
    <div class="col-sm-6"><?= $model->getFieldLabel('contact_group_id') ?>:</div>
    <div class="col-sm-6"><?= $model->contact_group_id ?></div>
</div>

<div class="row">
    <div class="col-sm-6"><?= $model->getFieldLabel('contact_group_parent_id') ?>:</div>
    <div class="col-sm-6"><?= is_object($model->parent) ? $model->parent->contact_group_parent_id : '-- Root Group --' ?></div>
</div>

<div class="row">
    <div class="col-sm-6"><?= $model->getFieldLabel('contact_group_name') ?>:</div>
    <div class="col-sm-6"><?= $model->contact_group_name ?></div>
</div>