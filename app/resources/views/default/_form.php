<?php
/**
 * @var \core\widgets\FormWidget $form
 * @var \app\models\Contact $model
 */

$groups = [null => '-- None Group --'];
$groups += (new \app\models\ContactGroup())->getStepsContactGroups();

$cityModel = new \app\models\City();
$_cities = $cityModel::find()->asArray()->all();
$cities = [];

foreach ($_cities as $city) {
    $cities[$city['city_id']] = $city['city_name'];
}

$form->field('selectbox', 'group_id', $model->groupID)->items($groups);

$form->field('text', 'firstname');
$form->field('text', 'lastname');
$form->field('selectbox', 'cityid')->items($cities);
$form->field('text', 'street');
$form->field('text', 'zip_code');

$form->button('submit', ($model->isNewRecord() ? 'Add Address' : 'Update Address'));

$form->render();