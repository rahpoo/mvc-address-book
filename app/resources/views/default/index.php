<?php
/**
 * @var int $by_group
 * @var \app\models\Contact $contactModel
 * @var \app\models\Contact[] $contacts
 */
$contactGroupComponent = new \app\components\ContactGroupComponent();
?>
<div class="row">
    <div class="col-sm-6">
        <h4>Contacts</h4>
    </div>
    <div class="col-sm-6 text-right">
        <a href="<?= urlTo('default/create') ?>" class="btn btn-success action-btn">New Contact</a>
        <a href="<?= urlTo('default/export?type=xml') ?>" class="btn btn-primary action-btn">XML Export</a>
        <a href="<?= urlTo('contact-groups/index') ?>" class="btn btn-default action-btn">Manage Groups</a>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Filter by groups</h3>
            </div>
            <div class="panel-body">
                <ul class="nav navbar">
                    <li><a href="index">All Contacts</a></li>

                    <?= $contactGroupComponent->getContactGroupsNavbar() ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        <?php
        \core\widgets\GridViewWidget::begin($contactModel, $contacts,
            [
                'id',
                'firstname',
                'lastname',
                'city.city_name',
                'street',
                'zip_code'
            ],
            [
                'table' => [
                    'class' => 'table table-striped table-hover'
                ],
                'header' => [
                    'style' => 'background: blue; color: #fff'
                ],
                'action_params' => ['field:id' => 'id', 'group_id' => $by_group]
            ])->render();
        ?>
    </div>
</div>

