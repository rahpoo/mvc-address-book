<?php
/**
* @var \app\models\Contact $model
*/
?>
<div class="row">
    <div class="col-sm-9">
        <h4>View address #<?= $model->id ?></h4>
    </div>
    <div class="col-sm-3">
        <a href="index" class="btn btn-default pull-right">Go To List</a>
        <a href="edit?id=<?= $model->id; ?>" class="btn btn-success action-btn">Edit</a>
        <a href="delete?id=<?= $model->id; ?>" class="btn btn-danger action-btn action-delete">Delete</a>
    </div>
</div>

<div class="row">
    <div class="col-sm-6"><?= $model->getFieldLabel('id') ?>:</div>
    <div class="col-sm-6"><?= $model->id ?></div>
</div>

<div class="row">
    <div class="col-sm-6"><?= $model->getFieldLabel('firstname') ?>:</div>
    <div class="col-sm-6"><?= $model->firstname ?></div>
</div>

<div class="row">
    <div class="col-sm-6"><?= $model->getFieldLabel('lastname') ?>:</div>
    <div class="col-sm-6"><?= $model->lastname ?></div>
</div>

<div class="row">
    <div class="col-sm-6"><?= $model->getFieldLabel('cityid') ?>:</div>
    <div class="col-sm-6"><?= $model->cityid ?></div>
</div>

<div class="row">
    <div class="col-sm-6"><?= $model->getFieldLabel('street') ?>:</div>
    <div class="col-sm-6"><?= $model->street ?></div>
</div>

<div class="row">
    <div class="col-sm-6"><?= $model->getFieldLabel('zip_code') ?>:</div>
    <div class="col-sm-6"><?= $model->zip_code ?></div>
</div>

<div class="row">
    <div class="col-sm-6"><?= $model->getFieldLabel('created_at') ?>:</div>
    <div class="col-sm-6"><?= $model->created_at ?></div>
</div>

<div class="row">
    <div class="col-sm-6"><?= $model->getFieldLabel('updated_at') ?>:</div>
    <div class="col-sm-6"><?= $model->updated_at ?></div>
</div>