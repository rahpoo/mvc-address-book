<html>
<head>

    <title><?= isset($title) ? $title : 'Meisam Ghanbari - Lodur TestCase2' ?></title>

    <link href="<?= urlTo('assets/bootstrap/dist/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?= urlTo('css/main.css') ?>" rel="stylesheet">

    <?= isset($head_css) ? $head_css : ''?>
    <?= isset($head_js) ? $head_js : ''?>
    <?= isset($head_styles) ? '<style type="text/css">' . $head_styles . '</style>' : ''?>

    <script language="JavaScript" type="text/javascript" src="<?= urlTo('js/jquery.min.js') ?>"></script>
    <script language="JavaScript" type="text/javascript" src="<?= urlTo('assets/bootstrap/dist/js/bootstrap.js') ?>"></script>
    <script language="JavaScript" type="text/javascript" src="<?= urlTo('js/general.js') ?>"></script>
    <?= isset($head_scripts) ? '<script language="JavaScript" type="text/javascript">' . $head_scripts . '</script>' : ''?>
</head>
<body style="background-color: #222; color: #eee">
    <div class="container">
        <?= isset($content) ? $content : '' ?>
    </div>

    <?= isset($js) ? $js : ''?>
    <?= isset($scripts) ? '<script language="JavaScript" type="text/javascript">' . $scripts . '</script>' : ''?>
</body>
</html>