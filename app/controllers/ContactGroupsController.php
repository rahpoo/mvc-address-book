<?php
/**
 * Created by PhpStorm.
 * User: meysam.ghanbari
 * Date: 2018-12-13
 * Time: 00:10
 */

namespace app\controllers;


use app\models\ContactGroup;
use core\base\Controller;

/**
 * Class ContactGroupGroupsController
 * @package app\controllers
 */
class ContactGroupsController extends Controller
{
    public function indexAction()
    {
        $contactGroup = new ContactGroup();
        $contactGroups = $contactGroup->getStepsContactGroups(null, false);

        $this->render('index', compact('contactGroup', 'contactGroups'));
    }

    public function createAction()
    {
        $model = new ContactGroup();

        if (isPost() && $model->load(post()) && $model->save()) {
            redirect('view?id=' . $model->attributes[$model::primaryKey()]);
        }

        return $this->render('create', compact('model'));
    }

    public function editAction($id = '')
    {
        if ($model = $this->find($id)) {
            if (isPost() && $model->load(post()) && $model->save()) {
                redirect('view?id=' . $id);
            }

            return $this->render('update', compact('model', 'id'));
        } else {
            redirect('page-not-found');
        }
    }

    public function viewAction($id = '')
    {
        if ($model = $this->find($id)) {
            return $this->render('view', compact('model', 'id'));
        }

        redirect('page-not-found');
    }

    public function deleteAction($id = '')
    {
        if ($model = $this->find($id)) {
            if ($model->delete())
                return ['status' => true, 'result' => "Address #$id has been deleted successfully."];
            else
                return ['status' => false, 'errorMsg' => "Deleting address #$id has been failed!"];
        }

        redirect('page-not-found');
    }
    
    protected function find($id)
    {
        return ContactGroup::findOneById($id);
    }
}