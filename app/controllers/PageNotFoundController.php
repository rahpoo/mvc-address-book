<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/28/2018
 * Time: 22:40 AM
 */

namespace app\controllers;


use core\base\Controller;

class PageNotFoundController extends Controller
{
    public $layout = 'black';

    public function indexAction()
    {
        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
        $this->render('index');
    }
}