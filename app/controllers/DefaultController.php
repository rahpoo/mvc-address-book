<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 10/29/2018
 * Time: 9:17 PM
 */

namespace app\controllers;


use app\models\Contact;
use app\models\ContactGroup;
use app\models\ContactGroupAssignment;
use core\base\Controller;

/**
 * Class DefaultController
 * @package app\controllers
 */
class DefaultController extends Controller
{
    public function indexAction()
    {
        $contactModel = new Contact();
        $by_group = get('by_group'); // filter contacts by this group id
        $group_contacts = [];

        if ($by_group) {
            // Get all contacts that assigned to this group
            $group_assignments = ContactGroupAssignment::find()->where('groupid', $by_group)->all();

            // Get all contacts of hierarchical parent groups
            foreach ($group_assignments as $group_assignment) {
                $group_contacts[] = $group_assignment->contactid;
            }
        }

        $contacts = $contactModel::find();

        // Filter contacts depends on selected group if was sent any group id by user
        if ($by_group)
            $contacts->where('id', 'in', $group_contacts);

        $contacts = $contacts->all();

        $this->render('index', compact('by_group', 'contactModel', 'contacts'));
    }

    public function createAction()
    {
        $model = new Contact();

        if (isPost() && $model->load(post()) && $model->save()) {
            redirect('view?id=' . $model->attributes[$model::primaryKey()]);
        }

        return $this->render('create', compact('model'));
    }

    public function editAction($id = '')
    {
        if ($model = $this->find($id)) {
            if (isPost() && $model->load(post()) && $model->save()) {
                redirect('view?id=' . $id);
            }

            return $this->render('update', compact('model', 'id'));
        } else {
            redirect('page-not-found');
        }
    }

    public function viewAction($id = '')
    {
        if ($model = $this->find($id)) {
            return $this->render('view', compact('model', 'id'));
        }

        redirect('page-not-found');
    }

    public function deleteAction($id = '', $group_id = '')
    {
        if ($model = $this->find($id)) {
            $assignments = $model->assignments;
            $total_assignments = count($assignments);

            $model->beginTransaction();

            // Delete self contact if is not exists in any group or only in one group
            // - or in a root group that has only one sub group
            if ($total_assignments <= 1 || ($total_assignments == 2 && (empty($group_id) || $assignments[0]->groupid == $group_id))) {
                if ($model->delete()) {
                    $model->commit();
                    return ['status' => true, 'result' => "Address #$id has been deleted successfully."];
                } else {
                    $model->rollback();
                    return ['status' => false, 'errorMsg' => "Deleting address #$id has been failed!"];
                }
            } else {
                /**
                 * @var ContactGroup $group
                 */
                $groupAssignment = $model->getAssignmentByGroupID($group_id);

                if ($groupAssignment) { // if this contact has this assigned group will be delete
                    $child = $groupAssignment->group->child;

                    if (!$groupAssignment->delete()) {
                        $model->rollback();
                        return ['status' => false, 'errorMsg' => "Deleting address #$id has been failed!"];
                    }

                    // if contact owner group has a direct child will be remove too
                    if ($child && ($childAssignment = $model->getAssignmentByGroupID($child->contact_group_id))) {
                        if (!$childAssignment->delete()) {
                            $model->rollback();
                            return ['status' => false, 'errorMsg' => "Deleting address #$id has been failed!"];
                        }
                    }
                }
            }

            // Get first assignment for set as owner of contact
            $newOwner = current($model->assignments);

            // if another assigned group was exists and isn't owner, will be convert to owner of this contact
            if ($newOwner && $newOwner->is_owner != 1) {
                $newOwner->is_owner = 1;

                if (!$newOwner->save()) {
                    $model->rollback();

                    return ['status' => false, 'errorMsg' => "Deleting address #$id has been failed!"];
                }
            }

            $model->commit();

            return ['status' => true, 'result' => "Address #$id has been deleted successfully."];
        }

        redirect('page-not-found');
    }

    /**
     * @param $id
     * @return null|Contact
     */
    protected function find($id)
    {
        return Contact::findOneById($id);
    }

    public function exportAction($type = 'xml')
    {
        $addressModels = Contact::find()->all();

        switch ($type) {
            default:
            case 'xml':
                $xml = new \SimpleXMLElement('<AddressBook/>');

                foreach ($addressModels as $addressModel) {
                    $address = $xml->addChild('address');

                    foreach ($addressModel->attributes() as $key => $value) {
                        $address->addChild($key, $value);
                    }
                }

                $file_name = 'Address Book [' . date('Y-m-d H-i-s') . '].xml';
                header('Content-type: text/xml');
                header('Content-Disposition: attachment; filename="' . $file_name . '"');
                echo $xml->asXML();

                break;
        }

        return;
    }
}